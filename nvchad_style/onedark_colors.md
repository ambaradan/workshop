# Onedark Colors Theme

## Base 30

-- +---------------------------------------------+
-- |  Color Name  |         RGB        |   Hex   |
-- |--------------+--------------------+---------|
-- | White        |                    | #abb2bf |
-- | darker_black |                    | #1b1f27 |
-- | black        |    nvim bg         | #1e222a |
-- | black2       |                    | #252931 |
-- | one_bg       |                    | #282c34 !
-- | one_bg2      |                    | #353b45 |
-- | one_bg3      |                    | #373b43 |
-- | grey         |                    ! #42464e |
-- | grey_fg      |                    | #565c54 |
-- | grey_fg2     |                    | #6f737b |
-- | light_grey   |                    | #6f737b |
-- | red          |                    | #e06c75 |
-- | baby_pink    |                    ! #de8c92 |
-- | pink         |                    ! #ff75a0 |
-- | line         | for lines          ! #31353d |
-- | green        |                    | #98c379 |
-- | vibrant_green|                    | #7eca9c |
-- | nord_blue    |                    | #81a1c1 |
-- | blue         |                    | #61afef |
-- | yellow       |                    | #e7c787 |
-- | sun          |                    | #ebcb8b |
-- | purple       |                    | #de98fd |
-- | dark_purple  |                    | #c882e7 |
-- | teal         |                    | #519aba |
-- | orange       |                    | #fca2aa |
-- | cyan         |                    | #a3b8ef |
-- | statusline_bg|                    | #22262e |
-- | light_bg     |                    | #2d3139 |
-- | pmenu_bg     |                    | #61afef |
-- | folder_bg    |                    | #61afef |
-- +---------------------------------------------+

## Base 16

-- +---------------------------------------------+
-- | 00           |                    | #1e222a |
-- | 01           |                    | #353b45 |
-- | 02           |                    | #3e4451 |
-- | 03           |                    | #545862 |
-- | 04           |                    | #565c64 |
-- | 05           |                    | #abb2bf |
-- | 06           |                    | #b5bdca |
-- | 07           |                    | #c8ccd4 |
-- | 08           |                    | #e06c75 |
-- | 09           |                    | #d19a66 |
-- | 0A           |                    | #e5c07b |
-- | 0B           |                    | #98c379 |
-- | 0C           |                    | #56b6c2 |
-- | 0D           |                    | #61afef |
-- | 0E           |                    | #c678dd |
-- | 0F           |                    | #be5046 |
-- +---------------------------------------------+
