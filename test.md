# Test Rocky Style

The term *Bind* is not correct and neither is *bind*. The correct term is BIND.

The same thing for *ram* or *Ram*. It should be written all in capital letters RAM.

!!!! Tip

    The style also controls Rocky admonitions; exclamation points must be three. However, it does not check internally for the correctness of the term *rocky*, the code block is excluded by default.

[Test](test_md)

The style also controls Rocky admonitions exclamation points must be three However it does not check internally for the correctness of the term the code block is excluded by default The style also controls Rocky admonitions exclamation points must be three However it does not check internally for the correctness of the term  the code block is excluded by default.
