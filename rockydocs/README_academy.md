# Rocky Linux Academy

## Scopo del progetto

Il progetto Rocky Linux Academy vuole arricchire i metodi di fruizione della documentazione su Rocky Linux aggiungendo la possibilità di utilizzare delle presentazioni per eventuali corsi sull'argomento.

## Software utilizzati

Il progetto utilizza [Marp](https://marp.app/) (Markdown Presentation Ecosystem), il software è scritto in linguaggio Javascript e rilasciato sotto la licenza MIT.

L'uso di questo ecosistema permette di creare le presentazioni desiderate da documenti scritti in linguaggio Markdown, il documento Markdown attraverso l'uso di *tag* standardizzati da **Marp** può essere velocemente adattato alle necessità specifiche della presentazione (il caso più comune è la divisione di un documento particolarmente lungo in più slides).

### Installazione di Marp

NOTA: L'installazione non è necessaria per gli utilizzatori di *VS Code* in quanto *Marp* viene fornito come estensione per questo editor.

L'installazione dell'ecosistema Marp fornisce una serie di strumenti per la trasformazione, la correttezza del documento e l'anteprima che consentono il pieno controllo della creazione delle presentazioni. Tutti questi strumenti vengono forniti da [Marp CLI](https://github.com/marp-team/marp-cli).
