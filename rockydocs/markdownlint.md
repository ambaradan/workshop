---
title: Markdownlint
author: Franco Colussi
contributors: Steven Spencer
tested with: 8.7, 9.1
tags:
  - markdown
  - editing
  - nvchad
---

# Markdownlint

Che cos'è un linter? In breve, un linter è uno strumento che aiuta a migliorare il codice. Il concetto di...

Che cos'è una linter? Una breve panoramica

Iniziamo definendo cos'è un linter. Come avete letto nell'introduzione, un linter è uno strumento che aiuta a migliorare il codice. Ma in che modo lo fa? La risposta è: analizzando il codice sorgente alla ricerca di problemi.

Il termine "linter" deriva da uno strumento originariamente chiamato "lint" che analizzava il codice sorgente C. L'informatico Stephen C. Johnson sviluppò questo strumento nel 1978, quando lavorava presso i Bell Labs.

Sia lo strumento lint originale, sia le utility simili precedenti, avevano l'obiettivo di analizzare il codice sorgente per proporre ottimizzazioni del compilatore. Col tempo, gli strumenti simili a lint hanno iniziato ad aggiungere molti altri tipi di controlli e verifiche.

Tuttavia, come abbiamo detto nell'introduzione, i linters non sono limitati ai linguaggi compilati. Al contrario: potremmo dire che i linters sono molto più preziosi per i linguaggi interpretati, poiché non c'è un compilatore che rilevi gli errori durante lo sviluppo.

Vantaggi del linting

Avete appena appreso la definizione di linteratura. Ora sapete anche quando questo tipo di strumento è stato inventato e da chi. Probabilmente avete già un'idea generale di ciò che un lintering può fare per il vostro codice. Ma in termini più pratici, quali sono i vantaggi che questo tipo di strumento può offrire? Eccone alcuni:

    Meno errori in produzione. L'uso dei liner aiuta a diagnosticare e risolvere i problemi tecnici (ad esempio, gli odori del codice) nel codice. Di conseguenza, un minor numero di difetti arriva in produzione.
    Codice leggibile, manutenibile e più coerente. Linters può aiutare i team a ottenere uno stile più leggibile e coerente, grazie all'applicazione delle sue regole.
    Meno discussioni sullo stile del codice e sulle scelte estetiche durante le revisioni del codice. Le discussioni sulla revisione del codice non dovrebbero essere costellate da discussioni infinite sulle preferenze stilistiche. I liner possono eliminare questi argomenti.
    Misurazione oggettiva della qualità del codice. Le discussioni sulla qualità del codice spesso sfociano nella soggettività. I linter forniscono una valutazione oggettiva e misurabile della qualità del codice.
    Codice più sicuro e performante. Non tutti i liner analizzano il codice sorgente in termini di prestazioni e sicurezza, ma alcuni lo fanno.
    La formazione sulla qualità del codice raggiunge un maggior numero di sviluppatori. I liner possono aiutare gli sviluppatori, soprattutto quelli più inesperti, a imparare la qualità del codice.

Tipi di controlli forniti dai linter

Come avete appena letto, lo strumento originale di lint analizzava il codice per consentire l'ottimizzazione dei compilatori, ma nel tempo sono stati rilasciati strumenti più avanzati e completi. Al giorno d'oggi esistono una miriade di linters diversi, che forniscono molti tipi di controlli. Vediamo rapidamente alcuni di essi.
Errori di sintassi

Il tipo di controllo più elementare e più vitale che un linter può fornire è la verifica degli errori di sintassi quando si tratta di JavaScript e di altri linguaggi interpretati. Gli sviluppatori non dovrebbero nemmeno inviare codice alla mainline senza che questo abbia superato la verifica della sintassi.

Aderenza agli standard di codifica

Un altro tipo di controllo vitale che i linters forniscono è l'aderenza agli standard di codifica. Alcuni potrebbero considerare questo aspetto come una questione puramente estetica, ma si sbaglierebbero. Avere un unico stile di codifica coerente è utile per ridurre il carico cognitivo della lettura, della comprensione e della manutenzione del codice. Una base di codice con uno stile coerente sarà più facile da capire e gli sviluppatori che la utilizzano avranno meno probabilità di introdurre bug.

Per questo motivo esistono dei linters specializzati nel verificare l'aderenza delle basi di codice agli stili di codice. Alcuni strumenti sono opinionati, cioè hanno un insieme predefinito di regole e convenzioni che non possono essere modificate. D'altro canto, esistono strumenti altamente configurabili, che consentono all'utente di adattare lo strumento ai propri stili di codifica preferiti.

Uno strumento per controllare i file markdown e segnalare i problemi di stile.

Markdownlint produce un elenco di problemi che trova e il numero di riga in cui si trova il problema. Vedere RULES.md per informazioni su ogni problema e su come correggerlo:

Stili

Non tutti scrivono markdown allo stesso modo e ci sono diversi gusti e stili, ognuno dei quali è valido. Sebbene le impostazioni predefinite di markdownlint producano file markdown che riflettono le preferenze di scrittura markdown dell'autore, il progetto potrebbe avere linee guida diverse.

Non è intenzione di markdownlint imporre uno stile specifico e per supportare questi diversi stili e/o preferenze, markdownlint supporta i cosiddetti "file di stile". Un file di stile è un file che descrive quali regole markdownlint deve abilitare e quali impostazioni applicare alle singole regole. Ad esempio, la regola MD013 controlla la presenza di righe lunghe e, per impostazione predefinita, segnala un problema per qualsiasi riga più lunga di 80 caratteri. Se il progetto ha un limite massimo di lunghezza delle righe diverso, o se non si vuole applicare un limite di righe, questo può essere configurato in un file di stile.

markdownlint-cli

Interfaccia a riga di comando per MarkdownLint

The CLI argument --config is not required. If it is not provided, markdownlint-cli looks for the file .markdownlint.jsonc/.markdownlint.json/.markdownlint.yaml/.markdownlint.yml in current folder, or for the file .markdownlintrc in the current or all parent folders. The algorithm is described in detail on the rc package page. Note that when relying on the lookup of a file named .markdownlintrc in the current or parent folders, the only syntaxes accepted are INI and JSON, and the file cannot have an extension. 

Configuration

markdownlint-cli reuses the rules from markdownlint package.

Configuration is stored in JSON, JSONC, YAML, or INI files in the same config format.

A sample configuration file:

{
  "default": true,
  "MD003": { "style": "atx_closed" },
  "MD007": { "indent": 4 },
  "no-hard-tabs": false,
  "whitespace": false
}

For more examples, see .markdownlint.jsonc, .markdownlint.yaml, or the style folder.


