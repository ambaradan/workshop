---
title: Linter & Formatter
author: Franco Colussi
contributors: Steven Spencer
tested with: 8.8, 9.2
tags:
  - nvchad
  - editor
  - markdown
---

# Linter & Formatter

## Obiettivo

Lo scopo di questa procedura è quello di sostituire le funzionalità di *linter* e *formatter* fornite dal plugin [null-ls](https://github.com/jose-elias-alvarez/null-ls.nvim), la sua sostituzione si rende necessaria in quanto il repository del plugin è stato archiviato e di conseguenza è cessato anche lo sviluppo.

Al momento della scrittura di questo documento esiste un fork [none-ls](https://github.com/nvimtools/none-ls.nvim) che ne dovrebbe garantire la continuazione.

Questa procedura permette di avere le funzionalità senza la necessità del supporto *null-ls* in *lspconfig*, rendendole indipendenti da LSP.

## Requisiti

- NvChad correttamente installato sul sistema
- Cartella *custom* installata da quella di esempio

## Introduzione

da fare

## Formatter

Per la parte riguardante la formattazione viene utilizzato il plugin [conform.nvim](https://github.com/stevearc/conform.nvim), il suo utilizzo consente la formattazione di numerosi tipi di file e nel caso specifico del linguaggio Markdown corregge in maniera automatica tutti gli errori comuni come la doppia riga vuota o lo spazio vuoto dopo la stringa.

Il suo utilizzo aiuta a produrre un file Markdown aderente alle specifiche.

### Pacchetti richiesti - conform.nvim

Per il funzionamento del plugin sono necessari due *formatter* installabili con *Mason*, la loro installazione viene fatta con il comando *MasonInstall* e sono i seguenti:

```text
:MasonInstall stylua
:MasonInstall prettier
```

Per inserire il plugin nella configurazione sarà necessario modificare due file della cartella `custom` e crearne uno nuovo, i due file da modificare sono *plugins.lua* e *mappings.lua* mentre il nuovo file da creare sarà *configs/comform.lua*.

### plugins.lua - conform.nvim

Il codice da inserire è il seguente:

```lua
  {
    "stevearc/conform.nvim",
    dependencies = { "mason.nvim" },
    cmd = "ConformInfo",
    event = "BufEnter",
    opts = {
      formatters_by_ft = {
        lua = { "stylua" },
        markdown = { "prettier" },
      },
      format_on_save = function()
        if vim.g.format_on_save then
          return {
            timeout_ms = 500,
            lsp_fallback = true,
          }
        end
      end,
    },
```

### mappings.lua - conform.nvim

Per richiamare la formattazione o attivare la formattazione al salvataggio si dovrà aggiungere la mappatura dei comandi in *mappings.lua*, il codice è il seguente:

```lua
-- Conform format
M.format = {
  plugin = true,
  n = {
    ["<leader>cf"] = {
      function()
        require("conform").format()
      end,
      "Format code",
    },
    ["<leader>tf"] = {
      "<cmd>ToggleFormatOnSave<cr>",
      "Toggle format on save",
    },
  },
}
```

La mappatura fornisce due scorciatoie per richiamare le funzioni che sono <kbd>Space</kbd> + <kbd>c</kbd> + <kbd>f</kbd> per attivare a comando la formattazione del file nel buffer e <kbd>Space</kbd> + <kbd>t</kbd> + <kbd>f</kbd> per attivare/disattivare la formattazione automatica al salvataggio del file.

### configs/conform.lua

Questo file non è presente e va creato nella cartella `configs` della vostra `custom`, una volta creato va popolato con il codice seguente:

```lua
return function(opts)
  vim.api.nvim_create_user_command("ToggleFormatOnSave", function()
    vim.g.format_on_save = not vim.g.format_on_save
  end, {
    desc = "Toggle format on save",
  })
  require("core.utils").load_mappings("format")
  require("conform").setup(opts)
end
```

## Linter

La funzionalità per il controllo della correttezza del codice (*lint*) è fornita mediante il plugin [nvim-lint](https://github.com/mfussenegger/nvim-lint). Il plugin permette il controllo di numerosi tipi di codice ma in questo documento ci limiteremo solo al codice  *markdown* utile per la scrittura della documentazione su Rocky Linux.

Nel caso siate interessati ad altri linguaggi è disponibile un [elenco di quelli supportati](https://github.com/mfussenegger/nvim-lint#available-linters) nella documentazione.

### Pacchetti richiesti - nvim-lint

Il pacchetto richiesto *markdownlint* e disponibile all'installazione in *Mason* e può essere installato con il comando:

```text
:MasonInstall markdownlint
```

Anche in questo caso sarà necessario modificare i file *plugin.lua* e *mapping.lua*, inoltre si renderà necessaria anche la creazione di un file di configurazione nella cartella `configs` e più precisamente *configs/lint.lua*.

### plugins.lua - nvim-lint

Il codice da inserire è il seguente:

```lua
  {
    "mfussenegger/nvim-lint",
    event = "VeryLazy",
    config = function()
      require "custom.configs.lint"
    end
  },

```

### mappings.lua - nvim-lint

Il codice da inserire è il seguente:

```lua
M.linter = {
  n = {
    ["<leader>cl"] = { "<cmd> lua require('lint').try_lint()<CR>", "Lint code" },
  },
}
```

La mappatura permette di attivare a comando il controllo del codice con la combinazione dei tasti <kbd>Space</kbd> + <kbd>c</kbd> + <kbd>l</kbd>.

### configs/lint.lua

In questo file vengono definiti i linter da utilizzare per il controllo del codice e il suo contenuto è il seguente:

```lua
require("lint").linters_by_ft = {
  markdown = { "markdownlint" },
}

vim.api.nvim_create_autocmd({ "BufWritePost" }, {
  callback = function()
    require("lint").try_lint()
  end,
})
```

Il codice inserito crea inoltre un autocomando per Neovim (`nvim_create_autocmd`) che permette il controllo sul codice a seconda dell'evento rilevato da Neovim. L'evento impostato attiva il controllo ad ogni salvataggio del file, è altresi possibile impostare altri eventi (`InsertLeave` o `TextChanged`) ma dai test eseguiti questa impostazione risulta la migliore rispetto al flusso di lavoro nella scrittura di codice Markdown.

### .markdownlintrc - da testare

Il linter *markdownlint* applica delle regole al suo controllo che possono essere [consulate in questa sezione](https://github.com/DavidAnson/markdownlint#rules--aliases), tuttavia queste regole sono molto stringenti e generano un numero rilevante di avvertimenti durante la scrittura della documentazione su Rocky Linux.

Per ovviare a questo problema si può utilizzare un file di configurazione personalizzato che va a modificare le impostazioni non conformi con il codice utilizzato (rappresentazione dei tasti della tastiera, titolo nel frontmatter, lunghezza massima della stringa, etc.).

Il file *.markdownlintrc* va creato nella radice della cartella utente per avere un'impostazione globale evitando così di aggiungere file ai repository.

```bash
touch ~/.markdownlintrc
```

Il file proposto è il seguente:

```json
{
  "default": true,
  "MD003": { "style": "atx" },
  "MD007": { "indent": 4 },
  "MD013": { "line_length": 450 },
  "MD025": { "front_matter_title": "" },
  "MD049": { "style": "asterisk" },
  "no-hard-tabs": false,
  "code-block-style": false,
  "no-inline-html": false
}
```

Questo dovrebbe eliminare tutti gli avvertimenti riguardo alle formattazioni proprie della documentazione Rocky Linux.

### controllo del contenuto con *vale*

Il plugin come detto in precedenza supporta numerosi linter e uno di questi è [vale](https://vale.sh/) che si occupa dei contenuti del documento, il linter ha bisogno dell'installazione dei vocabolari per poter funzionare. Per la sua installazione fare riferimento alla [pagina dedicata](https://docs.rockylinux.org/books/nvchad/vale_nvchad/) della Guida NvChad sulla documentazione Rocky Linux.

Una volta installato e configurato è sufficiente inserirlo nella tabella dei linter in *configs/lint.lua* in questo modo:

```lua
  markdown = { "markdownlint", "vale" },
```

Chiudere l'editor e riaprirlo e al salvataggio di un file Markdown verrà controllato da entrambi i linter.

## disabilitazione di *null-ls*

Questa operazione può essere effettuata anche dopo aver verificato che tutti funzioni come previsto in quanto i due plugin lavorano indipendentemente dai server LSP.

La disabilitazione consiste nel commentare e successivamente se tutto funziona eliminare il codice che inserisce il supporto *null-ls* a LSP, il plugin non viene richiesto da altri plugin e di conseguenza verrà evidenziato come da eliminare (*Clean*) in *Lazy*.

La parte da commentare è la seguente:

```lua
  {
    "neovim/nvim-lspconfig",
    -- dependencies = {
    --   -- format & linting
    --   {
    --     "jose-elias-alvarez/null-ls.nvim",
    --     config = function()
    --       require "custom.configs.null-ls"
    --     end,
    --   },
    -- },
    config = function()
      require "plugins.configs.lspconfig"
      require "custom.configs.lspconfig"
    end, -- Override to setup mason-lspconfig
  },

```

Una volta completate tutte le modifiche sarà sufficiente chiudere NvChad e riaprirlo per dare modo a *Lazy* di installare i plugin mancanti e se i linter e i formatter sono già installati si è pronti a lavorare.
