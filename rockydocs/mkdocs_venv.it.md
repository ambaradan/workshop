---
title: MkDocs in Virtual Enviroment
author: Franco Colussi
contributors: Steven Spencer
tested with: 8.7, 9.1
tags:
  - mkdocs
  - testing
  - documentation
---

# MkDocs (Python Virtual Enviroment)

## Introduzione

Uno degli aspetti del processo di creazione della documentazione per Rocky Linux è la verifica della corretta visualizzazione del nostro nuovo documento prima della sua pubblicazione.

Lo scopo di questa guida è quello di fornire alcuni suggerimenti per eseguire questa operazione in un ambiente python locale dedicato esclusivamente allo scopo.

La documentazione per Rocky Linux è scritta usando il linguaggio Markdown, un linguaggio progettato per essere convertito in altri formati, semplice nella sintassi e particolamente adatto alla scrittura di documentazione tecnica.

Nel nostro caso la documentazione viene convertita in HTML utilizzando un'applicazione python che si occupa della costruzione del sito statico. L'applicazione utilizzata dagli sviluppatori è [MkDocs](https://www.mkdocs.org/).

Uno dei problemi che si pongono durante lo sviluppo con una applicazione python è quello del suo isolamento dal sistema ospite, perchè se condivisa con il sistema, i suoi moduli potrebbero interferire con gli eventuali moduli installati nel sistema e darci degli errori di visualizzazione o dei malfunzionamenti del nostro sito locale.

Al riguardo ci sono già delle ottime guide che utilizzano i **container** per l'isolamento dell'interprete python, queste guide però implicano una conoscenza delle varie tecniche di containerizzazione sulle quali potremmo non essere così, o per niente, esperti.

In questa guida verrà utilizzato per la separazione il modulo `venv` fornito dal pacchetto *python* di Rocky Linux, questo modulo e disponibile su tutte le nuove versioni di *Python* dalla versione 3.6. In questo modo otterremo direttamente l'isolamento dell'interprete python sul sistema che stiamo usando senza la necessità di installare e configurare nuovi "**sistemi**".

### Requisiti

- una copia di Rocky Linux in esecuzione
- il pacchetto *python* correttamente installato
- familiarità con la riga di comando
- una connessione internet attiva

## Preparazione dell'ambiente

L'ambiente prevede una cartella principale dove contenere i due repository GitHub di Rocky Linux necessari e la cartella dove inizializzare e successivamente eseguire la nostra copia di python in ambiente virtuale.

Andremo quindi per prima cosa a creare la cartella che conterrà tutto il resto e contestualmente creeremo anche la cartella **env** dove eseguire MkDocs:

```bash
mkdir -p ~/lab/rockydocs/env
```

### Ambiente Virtuale Python

Per creare la nostra copia di Python dove far girare MkDocs utilizzeremo il modulo appositamente sviluppato per lo scopo, il modulo python `venv`, questo modulo consente la creazione di un ambiente virtuale, derivato da quello installato sul sistema, totalmente isolato e indipendente.

Questo ci consentirà di avere una copia in una cartella separata dell'interprete con solamente i pacchetti richiesti da MkDocs per l'esecuzione della documentazione di Rocky Linux installati.

Ci portiamo quindi nella cartella appena creata (*rockydocs*) e creiamo l'ambiente virtuale con:

```bash
cd ~/lab/rockydocs/
python -m venv env
```

Il comando popolerà la nostra cartella **env** con una serie di cartelle e file che riproducono l'albero di *python* del nostro sistema come mostrato sotto:

```text
env/
├── bin
│   ├── activate
│   ├── activate.csh
│   ├── activate.fish
│   ├── Activate.ps1
│   ├── pip
│   ├── pip3
│   ├── pip3.11
│   ├── python -> /usr/bin/python
│   ├── python3 -> python
│   └── python3.11 -> python
├── include
│   └── python3.11
├── lib
│   └── python3.11
├── lib64 -> lib
└── pyvenv.cfg
```

Come possiamo vedere l'interprete python utilizzato dall'ambiente virtuale rimane comunque quello disponibile sul sistema `python -> /usr/bin/python`, il processo di virtualizzazione si occupa solamente di isolare la nostra istanza.

#### Attivare l'ambiente virtuale

Tra i files elencati nella struttura sopra possiamo vedere che ci sono una serie di files denominati **activate** che servono a questo scopo, il suffisso di ogni file indica la *shell* relativa.

L'attivazione separa questa istanza di python da quella di sistema e consente di eseguire uno sviluppo della documentazione senza interferenze. Per attivarla ci portiamo nella cartella *env* ed eseguiamo il comando:

```bash
[rocky_user@rl9 rockydocs]$ cd ~/lab/rockydocs/env/
[rocky_user@rl9 env]$ source ./bin/activate
```

Abbiamo usato il comando *activate* senza alcun suffisso in quanto questo si riferisce alla shell *bash*, shell di default di Rocky Linux. A questo punto il nostro *shell prompt* dovrebbe essere cambiato in:

```bash
(env) [rocky_user@rl9 env]$
```

Come possiamo vedere la parte iniziale *(env)* indica che ci troviamo ora nell'ambiente virtuale. La prima cosa da fare a questo punto è quella di aggiornare **pip** il gestore dei moduli python che verrà utilizzato per l'installazione di MkDocs, per farlo usiamo il comando:

```bash
python -m pip install --upgrade pip
```

```bash
python -m pip install --upgrade pip
Requirement already satisfied: pip in ./lib/python3.9/site-packages (21.2.3)
Collecting pip
  Downloading pip-23.1-py3-none-any.whl (2.1 MB)
    |████████████████████████████████| 2.1 MB 1.6 MB/s
Installing collected packages: pip
  Attempting uninstall: pip
    Found existing installation: pip 21.2.3
    Uninstalling pip-21.2.3:
      Successfully uninstalled pip-21.2.3
Successfully installed pip-23.1
```

#### Disattivare l'ambiente

Per uscire dall'ambiente virtuale e sufficiente richiamare il comando *deactivate*

```bash
(env) [rocky_user@rl9 env]$ deactivate
[rocky_user@rl9 env]$
```

Come possiamo vedere il *prompt* del terminale dopo la disattivazione e ritornato ad essere quello di sistema, a riguardo si consiglia di controllare sempre con attenzione il prompt prima di eseguire l'installazione di *MkDocs* e i successivi comandi. Il controllo eviterà inutili e indesiderate installazioni globali dell'applicazione ed esecuzioni fallite di `mkdocs serve`.

### Preparare il materiale

Ora che abbiamo visto come creare il nostro ambiente virtuale  e come gestirlo possiamo passare a preparare tutto il necessario.

Per una corretta esecuzione di una versione locale della documentazione di Rocky Linux abbiamo bisogno di due repository, il repository della documentazione [documentation](https://github.com/rocky-linux/documentation) e quello della struttura del sito [docs.rockylinux.org](https://github.com/rocky-linux/docs.rockylinux.org), entrambi scaricabili dal GitHub di Rocky Linux.

Iniziamo quindi dal repository della struttura del sito che cloneremo nella cartella **rockydocs**:

```bash
cd ~/lab/rockydocs/
git clone https://github.com/rocky-linux/docs.rockylinux.org.git
```

In questa cartella ci sono due files che andremo ad utilizzare per la costruzione della documentazione locale e sono **mkdocs.yml** e **requirement.txt**, il primo è il file di configurazione che utilizzeremo per inizializzare MkDocs mentre il secondo contiene tutti i pacchetti python necessari per l'installazione di *mkdocs*.

Una volta terminato scarichiamo anche il repository della documentazione:

```bash
git clone https://github.com/rocky-linux/documentation.git
```

A questo punto avremo la seguente struttura nella cartella **rockydocs**:

```text
rockydocs/
├── env
├── docs.rockylinux.org
├── documentation
```

Schematizzando possiamo dire che la cartella **env** sarà il nostro motore *MkDocs* che utilizzerà **docs.rockylinux.org** come contenitore per visualizzare i dati contenuti in **documentation**.

### Installazione di mkdocs

Come abbiamo evidenziato sopra gli sviluppatori di Rocky Linux forniscono il file **requirement.txt** che contiene l'elenco dei moduli necessari ad una corretta esecuzione di una istanza MkDocs personalizzata, utilizzeremo il file per installare in una sola operazione tutto il necessario.

Per prima cosa entriamo nel nostro ambiente virtuale python:

```bash
[rocky_user@rl9 rockydocs]$ cd ~/lab/rockydocs/env/
[rocky_user@rl9 env]$ source ./bin/activate
(env) [rocky_user@rl9 env]$
```

E procediamo all'installazione di MkDocs e di tutti i suoi componenti con il comando:

```bash
(env) [rocky_user@rl9 env]$ python -m pip install -r ../docs.rockylinux.org/requirements.txt
```

Per verificare che tutto sia andato a buon fine possiamo richiamare l'help di MkDocs che ci introduce anche i comandi utilizzabili:

```bash
(env) [rocky_user@rl9 env]$ mkdocs -h
Usage: mkdocs [OPTIONS] COMMAND [ARGS]...

  MkDocs - Project documentation with Markdown.

Options:
  -V, --version  Show the version and exit.
  -q, --quiet    Silence warnings
  -v, --verbose  Enable verbose output
  -h, --help     Show this message and exit.

Commands:
  build      Build the MkDocs documentation
  gh-deploy  Deploy your documentation to GitHub Pages
  new        Create a new MkDocs project
  serve      Run the builtin development server
```

Se tutto a funzionato come previsto possiamo uscire dall'ambiente virtuale e iniziare a preparare i collegamenti necessari.

```bash
(env) [rocky_user@rl9 env]$ deacticate
[rocky_user@rl9 env]$
```

### Collegare la documentazione

Ora che abbiamo preparato tutto il necessario rimane solo da collegare il repository della documentazione all'interno del sito contenitore *docs.rockylinux.org*. A seguito della impostazione definita in *mkdocs.yml*:

```yaml
docs_dir: 'docs/docs'
```

Dovremo prima creare una cartella **docs** in **docs.rockylinux.org** e successivamente dal suo interno collegare la nostra cartella **docs** del repository **documentation**.

```bash
cd ~/lab/rockydocs/docs.rockylinux.org
mkdir docs
cd docs/
ln -s ../../documentation/docs/ docs
```

## Avviare la Documentazione locale

A questo punto siamo pronti per avviare la nostra copia locale della documentazione di Rocky Linux, per prima cosa dobbiamo avviare l'ambiente virtuale python e successivamente inizializzare la nostra istanza di MkDocs con le impostazioni definite in **docs.rockylinux.org/mkdocs.yml**.

Questo file contiene tutte le impostazioni necessarie alla localizzazione, alla gestione delle funzionalità ed alla gestione del tema.

Gli sviluppatori per la UI del sito hanno scelto il tema [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/) che fornisce numerose funzioni e personalizzazioni aggiuntive rispetto al tema di default di MkDocs.

Andiamo quindi ad eseguire i seguenti comandi:

```bash
cd ~/lab/rockydocs/env/
source ./bin/activate
(env) [rocky_user@rl9 env]$ mkdocs serve -f ../docs.rockylinux.org/mkdocs.yml
```

Dovremmo vedere nel nostro terminale l'inizio della costruzione del sito durante il quale ci verranno segnalati gli eventuali errori riscontrati da MkDocs come link mancanti o altro:

```text
INFO     -  Building documentation...
INFO     -  Adding 'de' to the 'plugins.search.lang' option
INFO     -  Adding 'fr' to the 'plugins.search.lang' option
INFO     -  Adding 'es' to the 'plugins.search.lang' option
INFO     -  Adding 'it' to the 'plugins.search.lang' option
INFO     -  Adding 'ja' to the 'plugins.search.lang' option
...
...
INFO     -  Documentation built in 102.59 seconds
INFO     -  [11:46:50] Watching paths for changes:
            '/home/rocky_user/lab/rockydocs/docs.rockylinux.org/docs/docs',
            '/home/rocky_user/lab/rockydocs/docs.rockylinux.org/mkdocs.yml'
INFO     -  [11:46:50] Serving on http://127.0.0.1:8000/
```

Aprendo ora il nostro browser all'indirizzo specificato sopra avremo la nostra copia del sito della documentazione in esecuzione, la copia rispecchia perfettamente il sito online per funzionalità e struttura consentendoci quindi di valutare l'aspetto e l'impatto che la nostra pagina avrà nel sito.

MkDocs integra un meccanismo per il controllo delle modifiche ai files della cartella specificata dal percorso `docs_dir` e quindi l'inserimento di una nuova pagina o la modifica di una esistente in **documentation/docs** verrà automaticamente riconosciuta e produrrà una nuova costruzione del sito statico.

Poichè il tempo per costruire il sito statico da parte di MkDocs può essere anche di alcuni minuti su computer non così potenti si consiglia di revisionare con attenzione la pagina che si stà scrivendo prima di salvarla o inserirla. Questo consente di risparmiare l'attesa mentre il sito viene ricostruito solo perchè ci siamo dimenticati ad esempio la punteggiatura.

### Uscita dall'Ambiente di sviluppo

Una volta soddisfatti di come la nostra nuova pagina verrà visualizzata nel sito possiamo uscire dal nostro ambiente di sviluppo, questo comporta prima l'uscita da *MkDocs* e successivamente la disattivazione dell'ambiente virtuale python. Per uscire da *MkDocs* dobbiamo digitare la combinazione di tasti <kbd>CTRL</kbd> + <kbd>C</kbd> mentre come abbiamo visto sopra per uscire dall'ambiente virtuale dovremo richiamare il comando `deacticate`.

```bash
...
INFO     -  [22:32:41] Serving on http://127.0.0.1:8000/
^CINFO     -  Shutting down...
(env) [rocky_user@rl9 env]$
(env) [rocky_user@rl9 env]$ deacticate
[rocky_user@rl9 env]$
```

## Conclusione e considerazioni funzionali

La verifica delle nostre nuove pagine in un sito locale di sviluppo ci fornisce la garanzia che il nostro lavoro risulterà sempre conforme al sito online della documentazione consentendoci quindi di contribuire in maniera ottimale.

La conformità del documento è inoltre un grande aiuto per i curatori del sito della documentazione che in questo modo dovranno occuparsi solo della correttezza dei contenuti.

Concludendo, possiamo dire che questo metodo permette di soddisfare i requisiti per una installazione "pulita" di MkDocs senza la necessità di ricorrere alla containerizzazione.
