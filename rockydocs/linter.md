---
title: Linter - nvim-lint
author: Franco Colussi
contributors: Steven Spencer
tested with: 8.8, 9.2
tags:
  - nvchad
  - editor
  - markdown
---

# nvim-lint - Gestore di Linter

## Introduzione ai Linter

Prima di iniziare a vedere l'uso dei linter in NvChad, è importante capire il significato del termine *lint*. Nella programmazione, lint viene usato per identificare del codice che non è ottimizzato, che è scritto male o che potrebbe avere qualche bug anche se tecnicamente corretto. Un linter è uno strumento dedicato a rilevare i lint nel codice.

Il linting è l'esecuzione di un *linter* sul codice per individuare gli errori, sia stilistici che di programmazione, consentendone così la loro correzione. Un linter può facilmente identificare e segnalare i potenziali bug, gli errori di sintassi e tutti i problemi legati alla programmazione (variabili inutilizzate, l'uso di funzioni non definite, ecc.).

In questa guida verrà affrontato l'argomento con la finalità di scrivere documentazione Rocky Linux conforme agli [standard Markdown](https://www.markdownguide.org/) e per lo scopo verrà utilizzato [markdownlint](https://github.com/igorshubovych/markdownlint-cli). Verrà inoltre illustrato l'uso del linter [vale](https://vale.sh/) per il controllo dei contenuti della documentazione.
