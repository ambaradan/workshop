---
title: Language Server Protocol
author: Franco Colussi
contributors: Steven Spencer
tested with: 8.6, 9.0
tags:
  - NvChad
  - coding
  - LSP
---

# LSP - DAP - Linters - Formatters

## Introduzione

I moderni editor di codice sono sviluppati per essere leggeri ed estensibili, progettati per fornire un ambiente di sviluppo integrato per i linguaggi di programmazione, questa integrazione viene fornita attraverso l'uso di estensioni o plugin specifici per i vari linguaggi. In questo modo è possibile configurare al meglio l'editor per lo scenario sul quale si stà lavorando, mantenendolo al contempo leggero e reattivo.

Il componente di supporto (estensione o plugin) analizza e compila il codice sorgente scritto dagli utenti nell'editor e fornisce una serie di funzioni di supporto allo sviluppo in base al contesto. Per soddisfare questi requisiti, gli sviluppatori dei linguaggi di programmazione hanno iniziato a sviluppare le estensioni per i loro linguaggi.

Al giorno d'oggi gli strumenti di sviluppo sono diventati un requisito essenziale per gli sviluppatori e *NvChad/Neovim*, come tutti gli editor moderni, supporta queste funzionalità, la loro integrazione però in un editor testuale come NvChad richiede solitamente l'intervento manuale dell'utente.

## Obiettivi

- Familiarizzare con i termini e i processi delle funzionalità avanzate
- Acquisire maggiore conoscenza dei processi interni del vostro editor

## Requisiti e Competenze

Per questo argomento non è necessaria alcuna competenza specifica se non quella di voler approfondire il funzionamento del vostro NvChad.

**Livello di difficoltà** :star:

**Tempo di lettura:** da stabilire

## LSP

Le funzionalità avanzate in NvChad vengono implementate attraverso uno scambio di messaggi tra il vostro editor e un processo di server linguistico, questa comunicazione avviene rispettando uno standard definito in un *Protocollo* il [Language Server Protocol](https://microsoft.github.io/language-server-protocol/).

È analogo ad altri protocolli come il più conosciuto HTTP, e si compone di due parti:

Un *server linguistico*: Un processo che fornisce le competenze linguistiche comunicando con gli strumenti di sviluppo.

Un *client linguistico*: Un editor di codice/strumento di sviluppo o un'estensione che può comunicare con un particolare Language Server.

Questo *protocollo* sviluppato originariamente da Microsoft&trade; per il suo [Visual Studio](https://visualstudio.microsoft.com/) è ora una protocollo aperto e nella sua standardizzazione hanno contribuito alcune delle società più importanti del settore tra cui RedHat&trade;.

Utilizzando LSP, gli editor di codice e i fornitori di strumenti linguistici si associano per fornire un insieme di funzionalità linguistiche intelligenti, come il completamento del codice, il passaggio alla definizione, l'azione del codice, ecc.

## DAP

Il [Debug Adapter Protocol](https://microsoft.github.io/debug-adapter-protocol/) definisce il protocollo utilizzato tra un editor o IDE e un debugger o un runtime. Queste funzionalità sono indirizzate principalmente ai linguaggi di programmazione (Go, Javascript, PHP, etc.) e non verranno trattate in questa guida. Per chi voglia approfondire questo argomento può iniziare dal [plugin DAP](https://github.com/mfussenegger/nvim-dap) per Neovim.

## Linters

Un linter è uno strumento che aiuta a migliorare il codice analizzando il codice sorgente alla ricerca di problemi. Anche se nati per il linguaggi di programmazione i *linters* sono molto utili anche per i linguaggi interpretati, visto che non c'è un compilatore che rilevi gli errori durante lo sviluppo (e questo è esattamente il caso del codice Markdown).

I linters consentono di produrre del codice più leggibile e più coerente, possono inoltre aiutare i team di documentazione ad ottenere uno stile più conforme fra i vari contributori, grazie all'applicazione delle sue regole. I linter inoltre possono aiutare gli sviluppatori, soprattutto quelli più inesperti, a valutare la qualità del proprio codice.

I controlli più comuni effettuati dai linters sono:

- Errori di sintassi: è il controllo più elementare e più importante che un linter può fornire quando si tratta di linguaggi interpretati.
- Aderenza agli standard di codifica: anche questo controllo è molto importante. Avere un unico stile di codifica coerente è utile per ridurre il carico cognitivo della lettura e della comprensione.
- Problemi potenziali: sono segnali che indicano che qualcosa potrebbe essere sbagliato nel codice,

## LSP in NvChad

NvChad fornisce un meccanismo automatico per l'installazione di server linguistici attraverso il plugin `williamboman/mason.nvim`; questo plugin fornisce un'interfaccia grafica per l'installazione dei [LSP](https://microsoft.github.io/language-server-protocol/) (Language Server Protocol), dei [DAP](https://microsoft.github.io/debug-adapter-protocol/) (Debug Adapter Protocol), i linter e i formatter.

![Mason UI](./images/mason_ui.png)

Per conoscere tutti i comandi disponibili, basta premere il tasto <kbd>g?</kbd> per avere a disposizione la legenda completa.

![Mason Help](./images/mason_help.png)

### Introduction to *nvim-lspconfig*

*nvim-lspconfig* è una raccolta di configurazioni, fornite dalla comunità, per il client server linguistico integrato nel nucleo di Nvim. Questo plugin fornisce quattro caratteristiche principali:

- comandi di avvio predefiniti, opzioni di inizializzazione e impostazioni per ogni server.
- un risolutore della directory principale che cerca di individuare la radice del progetto
- una mappatura automatica dei comandi che lancia un nuovo server linguistico o un server linguistico per ogni buffer aperto se fa parte di un progetto tracciato.
- comandi di utilità come LspInfo, LspStart, LspStop e LspRestart, per la gestione delle istanze del server linguistico.

L'uso combinato dei due plugin consente di scaricare i server linguistici necessari e di configurarli automaticamente da NvChad.

#### Downloading Language Servers

Per scaricare il server linguistico scelto, apriamo l'interfaccia di *Mason* dal nostro editor con il comando *Mason* dal nostro editor con il comando  <kbd>SHIFT</kbd> + <kbd>:Mason</kbd>. Supponiamo di voler installare il server linguistico per  *Markdown*. Per farlo, si digita <kbd>2</kbd> per passare alla sezione `LSP`.

Ora scendiamo con il tasto freccia fino a trovare il server linguistico `marksman`. Premendo la barra spaziatrice possiamo ottenere alcune informazioni sul server, come si può vedere nella schermata seguente.

![Mason Marksman](./images/mason_marksman.png)

Per installare il server, è sufficiente premere il tasto <kbd>i</kbd>; al termine dell'installazione Mason lo troverà tra i server installati.

![Marksman Installed](./images/mason_installed.png)

### Upgrading language servers

Come tutti gli altri componenti del nostro IDE, i server linguistici vengono costantemente migliorati e di conseguenza devono essere aggiornati.

L'aggiornamento, a differenza dell'installazione, è completamente automatico; per verificare se ci sono aggiornamenti disponibili è sufficiente aprire Mason con il comando <kbd>:Mason</kbd>. Per impostazione predefinita, a ogni apertura vengono controllati i rispettivi repository dei server linguistici installati e, se sono disponibili aggiornamenti, questi vengono evidenziati come nella seguente schermata.

![Mason Check Update](./images/mason_check_update.png)

A questo punto abbiamo la possibilità di aggiornare il server linguistico scelto andando alla riga corrispondente e digitando <kbd>u</kbd> oppure l'opzione di aggiornare tutti i server digitando <kbd>U</kbd> (maiuscolo); in entrambi i casi sarà *Mason* a occuparsi di scaricare il pacchetto aggiornato e di installarlo.

Per ottenere i dettagli sul pacchetto installato si possono consultare le informazioni fornite da *Mason*; per farlo basta posizionare il cursore sul server linguistico che si desidera controllare e premere <kbd>Invio</kbd>.

![Lua LS](./images/lua_language_server.png)

La sezione *Schema di configurazione del server LSP* fornisce informazioni dettagliate sulla configurazione del server nel nostro ambiente di sviluppo. Si tratta di informazioni puramente tecniche che difficilmente possono essere utili nell'uso quotidiano dell'editor, ma che tuttavia possono essere utili in caso di malfunzionamenti del server linguistico.

![LSP Server Schema](./images/lsp_server_schema.png)
