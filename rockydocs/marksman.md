---
title: Marksman
author: Franco Colussi
contributors: Steven Spencer
tested with: 8.8, 9.2
tags:
  - nvchad
  - editor
  - markdown
---

# Marksman - assistente per il codice

Durante la stesura del vostro documento per Rocky Linux può risultare utile uno strumento che permetta di inserire in maniera agevole i simboli necessari a definire i tag del linguaggio *markdown*, questo vi consentirà una scrittura più veloce e ridurrà la possibilità di errori di distrazione.

NvChad/Neovim include già dei widgets per il testo che aiutano nella scrittura come ad esempio la riproposizione delle parole più usate indicizzate per frequenza di inserimento, le nuove opzioni inserite da questo server linguistico andranno ad arricchire questi widgets.

Marksman si integra con il vostro editor per aiutarvi a scrivere e mantenere i vostri documenti Markdown utilizzando il [protocollo LSP](https://microsoft.github.io/language-server-protocol/), fornendo in questo modo funzionalità come il completamento, la definizione di goto, la ricerca di riferimenti, il refactoring dei nomi, la diagnostica e altro ancora.

## Obiettivi

- aumentare la produttività di NvChad nella scrittura di codice Markdown
- produrre documenti conformi alle regole del linguaggio Markdown
- affinare le proprie conoscenze riguardo al linguaggio

## Requisiti e Competenze

- Una conoscenza di base del linguaggio Markdown, consigliata la lettura della [Markdown Guide](https://www.markdownguide.org/)
- NvChad sulla macchina in uso con il [Template Chadr](./template_chadrc.md) installato correttamente

**Livello di difficoltà** :star:

**Tempo di lettura:** da stabilire

## Installazione di marksman

L'installazione del server linguistico non comporta particolari problemi in quanto è disponibile nativamente in **Mason**. E' possibile installarlo direttamente dalla *statusline* con il comando:

`:MasonInstall marksman`

Il comando aprirà l'interfaccia di *Mason* e installerà direttamente il server linguistico richiesto. Una volta terminata l'installazione del binario potete chiudere la schemata di *Mason* con il tasto <kbd>q</kbd>, la sua installazione tuttavia non comporta ancora la sua integrazione nell'editor, questa viene attivata attraverso la modifica del file `custom/configs/lspconfig.lua` del *Template Chadrc*.

## Integrazione nell'editor

!!! note "LSP in NvChad"

    L'integrazione dei server linguistici in NvChad è fornita dal plugin [nvim-lspconfig](https://github.com/neovim/nvim-lspconfig), questo plugin semplifica notevolmente il loro inserimento nella configurazione di NvChad.

Se durante l'installazione dell'editor avete scelto di installare anche il *Template Chadrc* questo avra creato nella vostra cartella `custom/configs` il file *lspconfig.lua*.

Questo file si occupa di inserire le chiamate necessarie all'utilizzo dei server linguistici ed inoltre permette di specificare quelli installati da voi. Per integrare *marksman* nella configurazione dei server linguistici dell'editor dovrete modificare la stringa dei *local servers* aggiungendo il vostro nuovo LSP. Aprite quindi il vostro NvChad sul file con il comando:

```bash
nvim ~/.config/nvim/lua/custom/configs/lspconfig.lua
```

E modificate la stringa dei *local servers* che al termine della modifica avrà il seguente aspetto:

```lua
local servers = { "html", "cssls", "tsserver", "clangd", "marksman" }
```

Salvate il file e chiudete l'editor con il comando `:wq`. Per verificare se il server linguistico viene correttamente attivato aprite un file markdown nel vostro NvChad e utilizzate il comando `:LspInfo` per visualizzare i server linguistici applicati a quel file, all'interno del riepilogo ci dovrebbe essere qualcosa come:

```text
 Client: marksman (id: 2, bufnr: [11, 156])
 	filetypes:       markdown
 	autostart:       true
 	root directory:  /home/your_user/your_path/your_directory
 	cmd:             /home/your_user/.local/share/nvim/mason/bin/marksman server
 
 Configured servers list: cssls, tsserver, clangd, html, yamlls, lua_ls, marksman
```

Questo indica che per il file aperto è stato attivato il server *marksman*, avviato in automatico `autostart: true` in quanto riconosciuto come file markdown `filetypes: markdown`. Le altre informazioni indicano il percorso dell'eseguibile usato per il controllo del codice `cmd:` e che questo viene usato in modalità server `marksman server` e inoltre che per i controlli viene usata la cartella radice `your_directory`.

!!! note "Cartella radice"

    Il concetto di "cartella radice" è importante nell'utilizzo di un server linguistico in quanto per effettuare i controlli sul documento, come collegamenti ad altri files o immagini per esempio, deve avere una "visione globale" del progetto. Possiamo dire che le "*cartelle radice*" possono essere equiparate ai "*Progetti*" presenti nei IDE grafici.

    La *cartella radice* chiamata anche "*working directory*" usata dall'editor per il file aperto può essere visualizzata con il comando `:pwd` e nel caso non corrisponda a quella desiderata può essere modificata con il comando `:lcd`, questo comando riassegna la *working directory* solo per quel buffer senza modificare eventuali impostazioni degli altri buffer aperti nell'editor.  

## Uso di marksman

Una volta completate tutte le operazione per il suo inserimento il server linguistico verrà attivato ogni qualvolta l'editor viene aperto su un file *markdown*, entrando nella modalità `INSERT` avrete alla digitazione di determinati caratteri delle nuove opzioni nei widgets che vi aiuteranno nella scrittura del documento, nella schermata sottostante potete vedere alcuni degli snippets markdown disponibili in questi widgets.

![Marksman Snippets](./images/marksman_snippets.png)

### Chiavi principali

Il server linguistico fornisce una serie di scorciatoie che attivano la scrittura assistita, questa comprende l'inserimento rapido di tag Markdown, la creazione di collegamenti e l'inserimento delle immagini nel documento. Di seguito verrà fornita una lista non esaustiva dei caratteri che attivano i vari snippets.

Questi snippets sono visualizzati all'interno di widgets che contengono anche altre scorciatoie, la navigazione del widget per la selezione di quelli forniti da *marksman* viene fatta con il tasto <kbd>Tab</kbd>.

| Chiave  | Snippets   |
|--------------- | --------------- |
| <kbd>h</kbd>   | Attiva l'inserimento rapido delle intestazioni dei titoli (da *h1* ad *h6*), inserendo ad esempio *h4* e premendo invio verranno inseriti quattro cancelletti e uno spazio e il cursore sarà già pronto in posizione per inserire il vostro titolo   |
| <kbd>b</kbd>   | La digitazione di questo carattere seguito da invio inserisce quattro asterischi e posiziona il cursore nel mezzo rendendo la scrittura della parte in **grassetto** molto più veloce    |
| <kbd>i</kbd>   | La digitazione di questo carattere seguito da invio inserisce due asterischi e posiziona il cursore in mezzo permettendo la scrittura rapida di testo in *corsivo*   |
| <kbd>bi</kbd>   | Questo chiave inserisce sei asterischi posizionando il cursore in mezzo per la scrittura di testo in ***grassetto corsivo***    |
| <kbd>img</kbd>   | Questa chiave inserisce la struttura markdown per l'inserimento di una immagine nel documento nel formato `![alt text](path)`. Da notare che la scrittura del percorso può essere fatta usando l'autocompletamento fornito dal server.   |
| <kbd>l</kbd>   | Questa chiave crea la struttura del tag markdown per un collegamento `[text](url)`. Anche in questo caso se il collegamento è riferito ad un file della **working directory** sarà possibile utilizzare l'autocompletamento e il server controllerà la correttezza del riferimento.    |
| <kbd>o</kbd> | La digitazione di questo carattere seguito da invio creerà una lista numerata di tre elementi per iniziare la creazione di una lista numerata  |
| <kbd>u</kbd> | Come per il carattere sopra la digitazione di questo carattere creerà una struttura per iniziare questa volta una lista non ordinata    |
| <kbd>q</kbd> | Questo carattere seguito da invio inserisce il tag per una citazione `>` seguito da uno spazio e posiziona il cursore per la scrittura della citazione |
| <kbd>s</kbd> | Questo carattere se usato da solo inserisce quattro tilde e posiziona il cursore in mezzo per la scrittura di testo ~~strikethrough~~ |
| <kbd>sup</kbd> | La chiave inserisce il tag *superscript*. Trademark<sup>TM |
| <kbd>sub</kbd> | La chiave inserisce il tag *subscript*. Note<sub>1 |
| <kbd>table</kbd> | Questa chiave abilita la creazione rapida della struttura di una tabella e permette di scegliere fra numerose strutture di partenza |
| <kbd>code</kbd> | Inserisce un blocco di codice in linea posizionando due backticks nella posizione in cui si trova il cursore posizionandolo al centro dei due backticks. |
| <kbd>codeblock</kbd> | Inserisce tre righe di cui due con i tripli backticks e una vuota dove inserire i vostri blocchi di codice. Da notare che inserisce anche la stringa *language* che va compilata con il linguaggio da voi usato nel blocco.

!!! note "Dichiarazione del blocco di codice"

    Le regole del codice Markdown consigliano di dichiarare sempre il codice utilizzato nel blocco anche in assenza di funzionalità di evidenziazione per una sua corretta interpretazione. Se il codice al suo interno e troppo generico si consiglia di usare "text" per la sua dichiarazione.

Le chiavi di attivazione comprendono anche altre scorciatoie che avrete modo di scoprire con l'utilizzo del server linguistico.

## Conclusioni

Anche se non strettamente necessario, questo server linguistico può con il tempo diventare un ottimo compagno nella vostra scrittura di documentazione per Rocky LInux. Con il suo utilizzo e di conseguenza la memorizzazione delle chiavi principali per l'inserimento dei simboli del codice Markdown consentirà una scrittura effettivamente più veloce consentendovi di concentrare l'attenzione sui contenuti.
