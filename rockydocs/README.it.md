# RockyDocs Ebooks

## Introduzione

Anche se viviamo in un mondo ormai "sempre connesso" ci possono essere delle situazioni nelle quali non abbiamo a disposizione la documentazione online di Rocky Linux. Pensiamo ad esempio ad una nuova installazione dove non viene riconosciuta o configurata automaticamente la scheda di rete o altre situazioni dove non sia possibile o conveniente consultare quella online.

In queste situazioni può risultare molto utile una copia "fisica" della documentazione da poter consultare, copia che grazie al formato può essere riprodotta su svariati dispositivi.

Avere un supporto PDF del documento ha inoltre il vantaggio di poter essere divulgato utilizzando qualsiasi sistema operativo sottostante e può essere facilmente utilizzato durante i corsi sull'argomento relativo. Questo aggiunge un indiscutibile valore didattico al progetto.

La sua portabilità inoltre ne consente una maggiore diffusione potendolo trasferire comodamente su un supporto, come una chiave USB, per la condivisione.

## Obiettivi

- Creare un sistema automatizzato per la generazione della documentazione ufficiale su Rocky Linux (in Inglese) in formato PDF che sarà disponibile al download.
- Consentire ai contributori, formatori e a chiunque sia interessato di generare il materiale della documentazione in formato PDF, consentendone inoltre la sua modifica (aspetto, contenuti, etc.) secondo le esigenze, sviluppando il tutto in un ambiente python personalizzato.

## Requisiti e competenze

- Un sistema Linux con i pacchetti *python* >= 3.6 e *Git* correttamente installati.
- Un clone della documentazione Rocky Linux
- Familiarità con la riga di comando
- Conoscenza di base della gestione dei repository con *git*

## Preparare l'ambiente

Per la creazione della documentazione in formato pdf avremo bisogno di una copia della documentazione Rocky Linux, che ci fornirà anche i file di questo repository e di un ambiente python dove eseguire l'applicazione (MkDocs) che si occuperà del processo. L'applicazione MkDocs verrà eseguita in un ambiente python virtuale per evitare eventuali interferenze con l'istanza python di sistema.

Per lo sviluppo dell'intero processo verrà utilizzata una cartella `rockydocs` nella propria cartella home, il nome e la posizione sono del tutto arbitrarie e possono essere modificate secondo le esigenze.

### Clonare la documentazione

La prima operazione da compiere consiste nel clonare il repository GitHub della documentazione per recuperare i file di questa cartella e i file *markdown* da convertire. Create quindi la cartella `rockydocs` e scaricate il clone con i seguenti comandi:

```bash
mkdir ~/rockydocs
cd ~/rockydocs
git clone https://github.com/rocky-linux/documentation.git
```

### Preparare l'ambiente virtuale

#### Creare l'ambiente

Ora che avete a disposizione localmente i file della documentazione online potete passare alla creazione del vostro "motore di conversione", per la creazione dell'ambiente virtuale python verrà utilizzato il modulo built-in **venv**. Per ulteriori approfondimenti potete consultare la [pagina dedicata della documentazione](https://docs.rockylinux.org/guides/contribute/localdocs/mkdocs_venv/).

```bash
cd ~/rockydocs
mkdir pdf_venv
python -m venv pdf_venv
```

L'ultimo comando creerà la struttura e i file necessari per eseguire un'istanza python completamente separata da quella di sistema e al termine avrete la seguente struttura nella vostra cartella `rockydocs`:

```text
├── documentation
│   ├── build_pdf
│   ├── contrib_templates
│   ├── crowdin.yml
│   ├── docs
│   ├── LICENSE.md
│   └── README.md
└── pdf_venv
    ├── bin
    ├── include
    ├── lib
    ├── lib64 -> lib
    └── pyvenv.cfg
```

#### Attivazione ambiente virtuale e installazione di MkDocs

Il prossima passo, una volta terminata la creazione dell'ambiente di sviluppo è l'attivazione dell'ambiente virtuale e l'installazione dell'applicazione python (MkDocs) necessaria alla conversione insieme al plugin [mkdocs-with-pdf](https://github.com/orzih/mkdocs-with-pdf) che aggiunge la funzionalità.

Attivare quindi l'ambiente virtuale python:

```bash
cd ~/rockydocs/pdf_venv
source ./bin/activate
```

A questo punto il prompt del terminale dovrebbe mostrare il nome della cartella racchiusa fra due parentesi rotonde `(pdf_env)` al posto del prompt di sistema. Questo indica che l'ambiente virtuale è stato attivato correttamente e che tutti i comandi python inseriti d'ora in poi verranno interpretati dalla nostra istanza python e non da quella di sistema.

Anche se non strettamente necessario e buona norma aggiornare il gestore dei pacchetti python (pip) prima di iniziare l'installazione, aggiornamento eseguibile con il comando:

```bash
(pdf_venv) python -m pip install --upgrade pip
```

Terminato l'aggiornamento è possibile passare all'installazione di MkDocs. Per automatizzare l'intero processo è stato creato un file `requirements.txt` che si trova nella cartella **build_pdf** della documentazione, il file contiene tutti i pacchetti necessari all'installazione e permette di installarli con un solo comando:

```bash
(pdf_venv) python -m pip install -r ../documentation/build_pdf/requirements.txt
```

Al termine possiamo verificare che l'installazione sia andata a buon fine con il comando `mkdocs -h` che dovrebbe mostrare una schermata di aiuto con le opzioni e i comandi disponibili.

## Creare il primo documento PDF

Per familiarizzare con il processo è possibile costruire uno dei documenti pdf già disponibili, i file di configurazione si trovano in `documentation/build_pdf/`. In questo esempio verrà utilizzato il file pdf della **Guida Amministratore** in inglese, per costruirlo sarà necessario utilizzare il file [yaml](https://yaml.org/) preparato allo scopo e presente in `documentation/build_pdf/admin_book/`:

```text
build_pdf/admin_book/
├── en.yml
├── it.yml
├── ko.yml
├── uk.yml
└── zh.yml
```

La sintassi per la costruzione è la seguente:

```bash
mkdocs build -f path_to_config_file.yml
```

Che trasferito in questo esempio si tradurrà in:

```bash
(pdf_venv) mkdocs -f ../documentation/build_pdf/admin_book/en.yml
```

Il processo inizierà con la creazione del sito statico della documentazione e al termine verrà creato il file pdf nella cartella `build_pdf/admin_book/pdf`, che come specificato nel file **en.yml** verrà chiamato *RockyLinuxAdminGuide.pdf*.

La corretta esecuzione del processo per questo documento conferma anche la corretta costruzione del vostro ambiente di sviluppo.

### Ulteriori letture

[mkdocs-with-pdf](https://github.com/orzih/mkdocs-with-pdf) Plugin per la generazione un singolo file PDF dal repository MkDocs.

[WeasyPrint](https://weasyprint.org/) WeasyPrint è una applicazione python che aiuta gli sviluppatori web a creare documenti PDF. Viene usata dal plugin per la trasformazione.
