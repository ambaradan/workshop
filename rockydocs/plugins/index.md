---
title: Usefull Plugins
author: Franco Colussi
contributors: Steven Spencer
tested with: 8.7, 9.1
tags:
  - nvchad
  - plugins
  - editor
---

# Usefull plugins

## Introduzione

La configurazione personalizzata creata dagli sviluppatori di NvChad consente di avere fin dalla sua prima installazione un ambiente integrato con molte delle funzionalità di un IDE grafico. Queste funzionalità, come avete visto, sono inserite nella configurazione di Neovim mediante dei plugins, quelli selezionati per NvChad dagli sviluppatori hanno la funzione di impostare l'editor per un uso generico.

L'ecosistema dei plugins per Neovim è però molto più vasto e mediante il loro uso consente di estendere l'editor per un utilizzo focalizzato al proprio scenario.

Lo scenario affrontato in questa sezione è quello della creazione di documentazione per Rocky Linux verranno quindi illustrati i plugins utili per la scrittura di codice Markdown, per la gestione dei repository Git e per altre attività che si riferiscono allo scopo.

## Requisiti

- NvChad correttamente installato sul sistema con il "template chadrc"
- Familiarità con la riga di comando
- Una connessione internet attiva

## Cenni generali sui plugins

Se avete scelto, durante l'installazione di NvChad, di installare anche il `template chadrc` avrete nella vostra configurazione una cartella **~/.config/nvim/lua/custom/**. Tutte le modifiche per gli inserimenti dei vostri plugins vanno fatte nel file **plugins.lua** presente in quella cartella, inoltre nel caso il plugin abbia bisogno di configurazioni aggiuntive queste vanno inserite nella cartella **/custom/configs**.

Neovim, sul quale si basa la configurazione di NvChad, non integra un meccanismo di aggiornamento della configurazione con l'editor in esecuzione. Questo implica che ad ogni modifica del file dei plugins è necessario terminare `nvim` e successivamente riaprirlo per avere le piene funzionalità del plugin.

L'installazione del plugin invece può essere eseguita subito dopo il suo inserimento nel file poichè il gestore dei plugins `lazy.nvim` tiene traccia delle modifiche in **plugins.lua**  e consente quindi la sua installazione "live".  
