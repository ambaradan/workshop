---
title: Markdown Preview
author: Franco Colussi
contributors: Steven Spencer
tested with: 8.7, 9.1
tags:
  - nvchad
  - plugins
  - editor
---

# Markdown Preview

## Introduzione

Durante la scrittura della vostra documentazione per Rocky Linux potreste aver bisogno di verificare la sua corretta visualizzazione una volta convertito in codice `HTML`.

Per integrare questa funzionalità nel vostro editor verranno illustrati, in questa pagina, due dei plugins disponibii allo scopo, [toppair/peek.nvim](https://github.com/toppair/peek.nvim) e [markdown-preview.nvim](https://github.com/iamcco/markdown-preview.nvim), entrambi supportano il *github-style*, la scelta del browser da utilizzare per l'anteprima e lo scorrimento sincronizzato con l'editor.

### Peek.nvim

Questo plugin utilizza [Deno](https://deno.com/manual) per il suo funzionamento, Deno è un runtime JavaScript, TypeScript e WebAssembly con impostazioni sicure predefinite. Per impostazione predefinita Deno non permette nessun accesso a file, rete o ambiente se non esplicitamente abilitato.

Se avete installato anche il `template chadrc` questo componente sarà gia disponibile in quanto è uno dei server linguistici installati di default.

Per installare il plugin dovrete editare il file **plugins.lua** aggiungendo il seguente blocco di codice:

```lua
    {
        "toppair/peek.nvim",
        build = "deno task --quiet build:fast",
        keys = {
            {
            "<leader>op",
                function()
                local peek = require("peek")
                    if peek.is_open() then
                peek.close()
                else
                peek.open()
                end
            end,
            desc = "Peek (Markdown Preview)",
            },
    },
        opts = { theme = "dark", app = "browser" },
    },
```

Una volta salvato il file potrete eseguire la sua installazione aprendo l'interfaccia del gestore dei plugins con il comando `:Lazy`. Il gestore dei plugins lo avrà già riconosciuto automaticamente e vi consentirà di installarlo digitando <kbd>I</kbd>.

Per avere le piene funzionalità però dovete chiudere NvChad (*nvim*) e riaprirlo, questo per consentire all'editor di caricare nella configurazione anche quelle di **Peek**.

La sua configurazione include già il comando per attivarlo `<leader> op` che sulla tastiera si traduce in <kbd>Space</kbd> + <kbd>o</kbd> seguito da <kbd>p</kbd>.

![Peek](./images/peek_command.png)

Avete inoltre la stringa:

```lua
        opts = { theme = "dark", app = "browser" },
```

Che vi permette di passare le opzioni per il tema chiaro/scuro dell'anteprima e il metodo da utilizzare per la visualizzazione.

In questa configurazione è stato scelto il metodo "browser" che apre il file da visualizzare nel browser di default del sistema ma il plugin consente attraverso il metodo "webview" di visualizzare un'anteprima del file utilizzando solamente **Deno** attraverso il componente [webview_deno](https://github.com/webview/webview_deno).  
