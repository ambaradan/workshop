# Linter & Formattter

## Obiettivi

- Scrivere documentazione per Rocky Linux rispettando la corretta sintassi del linguaggio Markdown.

## Requisiti

- NvChad correttamente installato sul sistema
- Cartella *custom* installata da quella di [esempio](https://github.com/NvChad/example_config)

## Introduzione

Markdown consente di scrivere e formattare rapidamente contenuti strutturati per il web. È facile da imparare e può essere usato per creare siti web, note, documentazione tecnica e persino libri! E' un linguaggio di markup leggero utilizzato per formattare documenti di testo semplice con una sintassi semplice e non invasiva.

Markdown viene definito dai loro creatori come:

> uno strumento di conversione da testo a HTML per chi scrive sul web. Markdown consente di scrivere utilizzando un formato di testo semplice e di facile lettura, per poi convertirlo in XHTML (o HTML) strutturalmente valido.

Markdown ha diverse varianti o flavors e implementazioni (note anche come parser). Ciascuna variante offre una serie di caratteristiche leggermente diverse. Possono includere combinazioni di sintassi di base ed estesa.

- La sintassi di base comprende paragrafi, intestazioni, caratteri o elenchi.
- La sintassi estesa include tabelle, ammonimenti, blocchi di codice e altre combinazioni a seconda della flavor utilizzata.

Trattandosi quindi di un documento che viene convertito in un altro formato risulta evidente la necessità di scriverlo nel modo più aderente possibile alla sua sintassi, questo per evitare che il parser che lo processerà possa generare degli errori durante la sua conversione restituendo così un documento incompleto o formattato in maniera errata.

Queste funzionalità in NvChad sono state fornite per lungo tempo dal plugin [null-ls](https://github.com/jose-elias-alvarez/null-ls.nvim) che però è stato archiviato e non verrà più sviluppato.
