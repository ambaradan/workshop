# Crowdin - parsing error

Crowdin's backend when converting the Markdown file to Xliff is not always so accurate, sometimes counting strings as "hidden" even though they should not be. This creates situations where with certain ways of viewing the Online Editor it is not possible to edit them.

An example is the screenshot (ukrainian_hidden_string.png), and this generates an incomplete translation in the final file (doc_online.png)

## Solve the problem

To solve the problem, we recommend reviewing the file in "Confortable" mode with approvals highlighted, as shown in the first screenshot, to detect the problem and then switch the mode to "Load basic list view" by selecting the "hidden strings" option to make the change (hidden_option.png)

In the list we will also find our string that should not have been hidden, and from there it will be possible to change its content (hidden_search.png)

This type of problem has been encountered numerous times and is currently unresolved. The solution while a workaround allows you to produce a fully translated document.
