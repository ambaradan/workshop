# Nvim-lint plugin

## plugins.lua

```lua
  {
    "mfussenegger/nvim-lint",
    event = "VeryLazy",
    config = function()
      require("custom.configs.lint")
    end,
  },
```

## configs/lint.lua

```lua
require("lint").linters_by_ft = {
  markdown = { "markdownlint", "vale" },
}

vim.api.nvim_create_autocmd({ "InsertLeave" }, {
  callback = function()
    require("lint").try_lint()
  end,
})
```
