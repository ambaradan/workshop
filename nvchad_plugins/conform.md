# Conform Plugin

## plugins.lua

```lua
  {
    "stevearc/conform.nvim",
    dependencies = { "mason.nvim" },
    cmd = "ConformInfo",
    event = "BufEnter",
    opts = {
      formatters_by_ft = {
        lua = { "stylua" },
      },
      format_on_save = function()
        if vim.g.format_on_save then
          return {
            timeout_ms = 500,
            lsp_fallback = true,
          }
        end
      end,
    },
    config = function(_, opts)
      local config = require("custom.configs.conform")
      config(opts)
    end,
  },
```

## configs/conform.lua

```lua
return function(opts)
  vim.api.nvim_create_user_command("ToggleFormatOnSave", function()
    vim.g.format_on_save = not vim.g.format_on_save
  end, {
    desc = "Toggle format on save",
  })
  require("core.utils").load_mappings("format")
  require("conform").setup(opts)
end
```

## mappings.lua

```lua
M.format = {
  plugin = true,
  n = {
    ["<leader>cf"] = {
      function()
        require("conform").format()
      end,
      "Format code",
    },
    ["<leader>tf"] = {
      "<cmd>ToggleFormatOnSave<cr>",
      "Toggle format on save",
    },
  },
}
```
