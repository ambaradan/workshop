# Neotree plugin

## plugins.lua

```lua
{
	"nvim-neo-tree/neo-tree.nvim",
	branch = "v3.x",
	lazy = false,
	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
		"MunifTanjim/nui.nvim",
	},
	config = function()
		require("custom.configs.neotree")
	end,
},
```

## configs/neotree.lua

```lua
require("neo-tree").setup({
	popup_border_style = "rounded",
	window = {
		position = "right",
	},
	icon = {
		folder_closed = "",
		folder_open = "",
		folder_empty = "ﰊ",
		-- The next two settings are only a fallback, if you use nvim-web-devicons and configure default icons there
		-- then these will never be used.
		default = "*",
		highlight = "NeoTreeFileIcon",
	},
})
```

## mapping.lua

```lua
-- Neotree Mapping
M.neotree = {
	n = {
		-- toggle
		["<C-n>"] = { "<cmd> Neotree right<CR>", "Neotree Toggle" },
		-- focus
		["<leader>e"] = { "<cmd> Neotree focus<CR>", "Neotree Focus" },
		-- float toggle
		["<leader>fl"] = { "<cmd> Neotree float<CR>", "Neotree Float" },
		-- float git_status
		["<leader>gg"] = { "<cmd> Neotree float git_status<CR>", "Neotree Git Status" },
	},
}
```
