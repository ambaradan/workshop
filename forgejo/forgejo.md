---
title: Forgejo su Rocky Linux
tags:
  - rockylinux
  - git
---

# Installare Forgejo su Rocky Linux 9

## Introduzione

Forgejo è una soluzione di hosting di codice leggera e gestita dalla comunità, scritta in Go. L'obiettivo di questo progetto è fornire un modo semplice e veloce per configurare un servizio Git self-hosted, funziona dovunque sia possibile compilare _Go_: Windows, macOS, Linux, ARM, ecc.

Forgejo ha requisiti minimi e può essere eseguito regolarmente anche su Raspberry Pi. Ci sono vari metodi per l'installazione ben descritti nella [loro documentazione](https://docs.codeberg.org/) che consentono l'installazione in molti scenari. In questa guida useremo il binario precompilato, la scelta è dovuta all'hardware utilizzato (una SBC - _Single-board Computer_). La guida è stata testata su un'installazione headless di Rocky Linux 9 su Raspberry Pi4 con 4GB di RAM ma dovrebbe funzionare anche su sistemi _x86_64_ in quanto i pacchetti e le istruzioni sono comuni.

## Requisiti

- Un server Rocky Linux 9 in esecuzione e connesso a internet
- Permessi di amministratore (come utente root o con _sudo_)
- Famigliarità con la riga di comando
- Un dominio o sottodominio FQDN - Fully Qualified Domain Name (classe A)

**NOTA:** Per il dominio si può usufruire di uno dei sottodomini gratuiti offerti da [noip.com](https://www.noip.com). I sottodomini di questo servizio soddisfano perfettamente i requisiti per l'installazione.

**NOTA:** Questa guida non comprende l'apertura delle porte del vostro router verso l'esterno necessarie al funzionamento, l'apertura varia a seconda dell'hardware a disposizione. Le porte da aprire dal vostro _host_ verso l'esterno sono la porta 80 (http), la porta 443 (https) e la porta 3000 (forgejo). La porta 3000(forgejo) una volta terminata l'installazione può essere richiusa.

## Installazione Software Richiesti

L'installazione prevede di appoggiarsi al database PostgreSQL per la gestione dei dati, questo per consentire di sviluppare, se necessario, un servizio con quantità di dati rilevanti. L'installazione prevede inoltre l'uso del server _nginx_ per lo sviluppo del Proxy Server che ci garantirà un grado di sicurezza più elevato. Chiaramente essendo di base un servizio _git_ avremo bisogno anche dell'eseguibile. Andiamo ad installare i pacchetti richiesti con:

```bash
dnf install git postgresql postgresql-server nginx wget
```

## Configurazione

Terminata l'installazione dei software richiesti possiamo passare alla configurazione del nostro ambiente.

### Creare l'Utente

Per prima cosa creiamo un utente di sistema che verrà utilizzato per tutte le operazioni del servizio con:

```bash
useradd \
    --system \
    --shell /bin/bash \
    --create-home \
    --comment `Forgejo User` \
    --home-dir /home/forgejo forgejo
```

### Creazione Struttura

Una volta creato l'utente passiamo alla creazione delle cartelle che conterranno i nostri file.

```bash
mkdir -p /var/lib/forgejo/{data,log} /etc/forgejo /run/forgejo
```

I dati e i file di log verranno salvati in `/var/lib/forgejo` e verranno utilizzate rispettivamente `/etc/forgejo` per il file di configurazione e `/run/forgejo` per il collegamento con il Socket Unix.

### Impostazioni Proprietario e Permessi

Create le directory dobbiamo ora assegnarle al nostro utente _forgejo_ e configurarne i permessi per rendere l'installazione più sicura.

```bash
chown -R forgejo:forgejo /var/lib/forgejo
chown -R forgejo:forgejo /var/run/forgejo
chown -R root:forgejo /etc/forgejo
chmod -R 750 /var/lib/forgejo
chmod 770 /etc/forgejo
```

> I permessi della cartella `/etc/forgejo` sono stati mantenuti intenzionalmente più permissivi per permettere all'installazione di Forgejo di scrivere il file di configurazione e verranno rinforzati successivamente.

### Politiche Firewall

```bash
firewall-cmd --add-port 80/tcp --add-port 443/tcp --permanent
firewall-cmd --reload
```

L'apertura seguente della porta `3000` servirà solo per l'installazione iniziale. Verrà in seguito modificata per utilizzare un socket Unix di conseguenza non utilizzeremo la flag `--permanent`.

```bash
firewall-cmd --add-port 3000/tcp
```

## Impostazione Database

### Inizializzare il Database

```bash
postgresql-setup --initdb --unit postgresql
```

### Abilitare il Servizio

```bash
systemctl enable --now postgresql.service
```

### Accedere al Database

Per accedere al database:

```bash
sudo -u postgres psql
```

Creare quindi un ruolo utente e un database da utilizzare per Forgejo:

```postgresql
postgres=# CREATE ROLE forgejo LOGIN ENCRYPTED PASSWORD 'your_password';
postgres=# CREATE DATABASE forgejo;
postgres=# GRANT ALL PRIVILEGES ON DATABASE forgejo TO forgejo;
postgres=# \q
```

Aprire il file di configurazione dell'autenticazione client di Postgres:

```bash
vi /var/lib/pgsql/data/pg_hba.conf
```

```bash
# IPv4 local connections:
host    forgejo           forgejo           127.0.0.1/32            md5
```

Salvare il file con `:wq` e riavviare il servizio:

```bash
systemctl restart postgresql.service
```

## Installare forgejo

Scaricare la versione appropriata per il proprio sistema dalla pagina di [Download](https://codeberg.org/forgejo/forgejo/releases.)

**NOTA:** la nomeclatura dei file da scaricare si discosta un pò da quella di Rocky Linux e dovremo scegliere `amd64` per un sistema _x86_64_ e `arm64` per una _aarch64_.

```bash
wget https://codeberg.org/forgejo/forgejo/releases/download/v1.18.5-0/forgejo-1.18.5-0-linux-arm64 -O /usr/local/bin/forgejo
```

A questo punto scarichiamo anche il file di verifica:

```bash
wget https://codeberg.org/forgejo/forgejo/releases/download/v1.18.5-0/forgejo-1.18.5-0-linux-arm64.sha256
```

E verifichiamo l'integrità del file scaricato con il comando:

```bash
sha256sum -c forgejo-1.18.5-0-linux-arm64.sha256 
forgejo-1.18.5-0-linux-arm64: OK
```

Questa è la versione più recente al momento della scrittura di questa guida ma adattate il comando alla versione secondo necessità.

Impostare i permessi corretti al file scaricato:

```bash
chmod 755 /usr/local/bin/forgejo
```

Per poter gestire il servizio dobbiamo creare l'_unità systemd_, che ci permetterà di avviarlo al boot e se necessario riavviarlo. Creiamo quindi un nuovo file _unit_ nella directory predefinita per i file `.service` di sistema con:

```bash
vi /usr/lib/systemd/system/forgejo.service
```

e seguendo le indicazioni fornite dal file di esempio [forgejo.service](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/contrib/systemd/forgejo.service) disponibile sul sito del progetto inseriamo i seguenti parametri:

```bash
[Unit]
Description=Forgejo (Beyond coding. We forge.)
After=syslog.target
After=network.target

# Postgresql Database Selection
Wants=postgresql.service
After=postgresql.service

# User Database and Working Directory Setup
[Service]
RestartSec=2s
Type=simple
User=forgejo
Group=forgejo
WorkingDirectory=/var/lib/forgejo/

# Persistence of /run/forgejo - Unix Socket
RuntimeDirectory=forgejo

# Execution of the Service
ExecStart=/usr/local/bin/forgejo web --config /etc/forgejo/app.ini
Restart=always
Environment=USER=forgejo HOME=/home/forgejo GITEA_WORK_DIR=/var/lib/forgejo

[Install]
WantedBy=multi-user.target
```

Ricaricare le unità di _systemd_ per inserire la nuova creata e abilitare il servizio al boot:

```bash
systemctl daemon-reload
systemctl enable forgejo.service
```

## Configurare Forgejo

Per la configurazione iniziale utilizzeremo l'installer web fornito da Forgejo. Avviamo quindi il servizio con:

```bash
systemctl start forgejo.service
```

e controlliamo che sia stato regolarmente avviato con:

```bash
systemctl status forgejo
● forgejo.service - Forgejo (Git with a cup of tea)
     Loaded: loaded (/etc/systemd/system/forgejo.service; enabled; vendor preset: disabled)
     Active: active (running) since Thu 2022-09-22 00:00:07 UTC; 2 weeks 2 days ago
   Main PID: 301 (forgejo)
      Tasks: 15 (limit: 23842)
        CPU: 2min 40.878s
     CGroup: /system.slice/forgejo.service
             └─301 /usr/local/bin/forgejo web -c /etc/forgejo/app.ini
```

Apriamo il seguente indirizzo in una pagina del nostro browser:

```text
http://your_domain:3000/install
```

Nella pagina di configurazione andiamo a compilare i campi con i seguenti parametri:

- Database Type: PostgreSQL
- Host: 127.0.0.1:5432
- Username: forgejo
- Password: Enter the password you chose during Postgres role creation.
- Database Name: forgejo
- SSL: Disable
- Site Title: Title of your choice.
- Repository Root Path: /var/lib/forgejo/data/repositories
- Git LFS Root Path: /var/lib/forgejo/data/lfs
- Run As Username: forgejo
- SSH Server Domain: your_domain
- SSH Server Port: 22
- Forgejo HTTP Listen Post: 3000
- Forgejo Base URL: https://your_domain/
- Log Path: /var/lib/forgejo/log

Configurare l'e-mail e le altre impostazioni come si ritiene opportuno, quindi fare clic su "Installa Forgejo". Verrete reindirizzati a un URL errato. Questo è normale, poiché non abbiamo ancora configurato Nginx o HTTPS. Prima però per motivi di prestazioni configureremo Forgejo per ascoltare su un socket unix invece che sulla porta TCP predefinita.

Prima di proseguire con le modifiche dobbiamo arrestare il servizio:

```bash
systemctl stop forgejo.service
```

Apriamo il file di configurazione:

```bash
vi /etc/forgejo/app.ini
```

E modifichiamo la sezione `[server]` rimuovendo ls stringa:

```text
HTTP_PORT = 3000
```

e aggiungendo le seguenti:

```text
HTTP_ADDR        = /run/forgejo/forgejo.sock
PROTOCOL         = unix
UNIX_SOCKET_PERMISSION = 666
```

Ora possiamo modificare i permessi della cartella `/etc/forgejo/` per rendere più sicura la nostra installazione:

```bash
chmod 750 /etc/forgejo
chown root:forgejo /etc/forgejo/app.ini
chmod 640 /etc/forgejo/app.ini
```

In questo modo solo i membri del gruppo _forgejo_ avranno il permesso di lettura sul file _app.ini_.

## NGINX - Reverse Proxy Setup

Ora dopo aver configurato l'installazione di Forgejo passiamo alla configurazione del server _nginx_ che ci fornirà il server proxy per la connessione, l'uso di un server di questo tipo aiuta ad elevare il livello di sicurezza della connessione.

Per fare questo abbiamo bisogno di un file di configurazione aggiuntivo in modo da mantenere pulito quello di default fornito da _nginx_, andiamo quindi a creare un nuovo file in `/etc/nginx/conf.d/`:

```bash
vi /etc/nginx/conf.d/forgejo.conf
```

e inseriamo il seguente codice:

```nginx
server {
        listen 80;
        listen [::]:80;
        server_name your_domain;
        return 301 https://$server_name$request_uri;
	access_log /var/log/nginx/forgejo-proxy_access.log;
	error_log /var/log/nginx/forgejo-proxy_error.log;
}
server {
        listen 443 ssl;
        listen [::]:443 ssl;
        server_name your_domain;
        ssl on;
        ssl_certificate /etc/letsencrypt/live/your_domain/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/your_domain/privkey.pem;
        location / {
                proxy_pass http://unix:/var/run/forgejo/forgejo.sock;
	}
	access_log /var/log/nginx/forgejo-proxy_access.log;
	error_log /var/log/nginx/forgejo-proxy_error.log;
```

La configurazione contiene già la sezione per l'attivazione di un server HTTPS (usando quindi una connessione cifata) del quale però non abbiamo ancora richiesto i certificati, questi verranno richiesti nel passo successivo.

Analizzando il file di configurazione possiamo dire che il primo `blocco server` serve a reindirizzare tutte le richieste _http_ verso la porta _https_ (443) mentre il `secondo blocco` si occupa di inoltrare le connessioni al Socket Unix sul quale abbiamo configurato il servizio Forgejo.

## HTTPS - letsencrypt

Ormai al giorno d'oggi qualunque browser si aspetta di scambiare i dati attraverso una connessione cifata quindi andremo a richiedere i certificati _LetsEncrypt_ per fornire una connessione _https_ al nostro Forgejo.

Installiamo quindi `certbot` che ci permetterà di attraverso una procedura semi automatica di ottenere i certificati per poter firmare in modo sicuro le nostre connessioni. Il pacchetto viene fornito dal repository `epel`:

```shell
dnf install certbot python3-certbot-nginx
```

```shell
certbot certonly --nginx
```

```text
Saving debug log to /var/log/letsencrypt/letsencrypt.log

Which names would you like to activate HTTPS for?
We recommend selecting either all domains, or all domains in a VirtualHost/server block.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
1: your_domain_here
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Select the appropriate numbers separated by commas and/or spaces, or leave input
blank to select all options shown (Enter 'c' to cancel):
```

Let's Encrypt verificherà la proprietà del dominio prima di emettere il certificato. Il certificato, la chain e la chiave privata saranno memorizzati in `/etc/letsencrypt/live/your_domain/`

```text
/etc/letsencrypt/live/
├── your_domain
│   ├── cert.pem -> ../../archive/your_domain/cert1.pem
│   ├── chain.pem -> ../../archive/your_domain/chain1.pem
│   ├── fullchain.pem -> ../../archive/your_domain/fullchain1.pem
│   ├── privkey.pem -> ../../archive/your_domain/privkey1.pem
│   └── README
└── README
```

Come possiamo vedere i percorsi coincidono con le impostazioni fatte nel file di configurazione di _nginx_. Terminata la configurazione verifichiamo che il nostro file _forgejo.conf_ non abbia errori che possano compromettere il corretto avvio del server con:

```shell
nginx -t
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

Se tutto è a posto possiamo avviare il server _nginx_ e il servizio _forgejo_ con il comando:

```shell
systemctl start nginx.service forgejo.service
```

E verificare che siano regolarmente in funzione con `systemctl status name_of_service`.

!!! warning "Attenzione"

    Anche se i servizi sono attivi il tentativo di accedere al nostro _forgejo_ dal web fallirà con un errore _HTTP 502_, questo perchè il sistema ha SELinux impostato su `enforcing` e di conseguenza non permette a nginx di scrivere sul filesystem nè di connettersi al socket forgejo.

### Politiche SElinux

Tutti gli accessi rifiutati da SELinux vengono memorizzati nel file di log `/var/log/audit.log` ma la consultazione non è delle più semplici. Per una lettura più comprensibile dei problemi riscontrati da SELinux possiamo utilizzare lo strumento `audit2why` fornito dal pacchetto _policycoreutils-python-utils_, installabile se non ancora presente nel sistema con:

```shell
dnf installa policycoreutils-python-utils
```

Una volta installato possiamo passare il contenuto del file di log allo strumento che ci restituirà un output decisamente più comprensibile. Il comando è il seguente:

```shell
grep http /var/log/audit/audit.log | audit2why
```

E questo è un'estratto del risultato:

```text
type=AVC msg=audit(1665224743.677:9869): avc:  denied  { write } for  pid=430 comm="nginx" name="forgejo.sock" dev="tmpfs" ino=851 scontext=system_u:system_r:httpd_t:s0 tcontext=system_u:object_r:var_run_t:s0 tclass=sock_file permissive=1
....
type=AVC msg=audit(1665224743.677:9869): avc:  denied  { connectto } for  pid=430 comm="nginx" path="/run/forgejo/forgejo.sock" scontext=system_u:system_r:httpd_t:s0 tcontext=system_u:system_r:unconfined_service_t:s0 tclass=unix_stream_socket permissive=1
....
```

Come possiamo vedere il server _nginx_ ha bisogno dei permessi di scrittura sul filesystem e di potersi connettere al socket Unix per poter funzionare regolarmente, entrambi però sono bloccati dalle politiche di sicurezza di SELinux. Per integrare queste politiche ci viene in aiuto lo strumento `audit2allow` sempre fornito dalle _policycoreutils-python-utils_. Per prima cosa visualizziamo a terminale il contenuto del file che verrà inserito con:

```shell
grep nginx /var/log/audit/audit.log | audit2allow  -a
```

Il risultato dovrebbe essere:

```text
module nginx 1.0;

require {
	type var_run_t;
	type unconfined_service_t;
	type httpd_t;
	class sock_file write;
	class unix_stream_socket connectto;
}

#============= httpd_t ==============
allow httpd_t unconfined_service_t:unix_stream_socket connectto;
allow httpd_t var_run_t:sock_file write;
```

Per inserire le nuove politiche abbiamo bisogno di creare un file `.te` (Type Enforcement policy) e successivamente compilarlo per inserirlo in SELinux. Creiamo quindi il file _.te_ con:

```shell
grep nginx /var/log/audit/audit.log | audit2allow -m nginx > nginx.te
```

Lo compiliamo con:

```shell
grep nginx /var/log/audit/audit.log | audit2allow -M nginx
```

E infine lo inseriamo e verifichiamo il corretto inserimento con:

```shell
semodule -i nginx.pp
semodule -l | grep nginx
nginx
```

Ora se ripetiamo il comando usato in precedenza per visualizzare l'anteprima del file _.te_ otterremo il seguente output:

```
grep nginx /var/log/audit/audit.log | audit2allow  -a

#============= httpd_t ==============

#!!!! This avc is allowed in the current policy
allow httpd_t unconfined_service_t:unix_stream_socket connectto;

#!!!! This avc is allowed in the current policy
allow httpd_t var_run_t:sock_file write;
```

A questo punto se apriamo una pagina del nostro browser all'indirizzo `https://your_domain` si presenterà la pagina principale di Forgejo, l'installazione non contiene ancora nessun utente, dovremo registrare il primo utente che diventerà anche l'amministratore del servizio. Una volta completata la registrazione saremo pronti per creare il nostro primo repository e cominciare a scrivere codice.
