# Aggiornamento NvChad

Per prima cosa devi controllare se la tua configurazione è vecchia e contiene ancora il plugin `extensions` nella configurazione. Per farlo e sufficiente aprire NvChad e controllare gli aggiornamenti dei plugin con `:Lazy` seguito da <kbd>Ctrl</kbd> + <kbd>C</kbd>, se ricevi la segnalazione che non è più raggiungibile è necessario chiudere l'editor ed eliminare le cartelle **ui** e **extensions** dal percorso di installazione di *Lazy* `~/.local/share/nvim/lazy/`.

```bash
rm -rf ~/.local/share/nvim/lazy/{ui,extensions}
```

Aprire *NvChad* che si aprirà senza la *tabufline* e la *statusline* chiaramente, eseguire l'aggiornamento con `:NvChadUpdate` che si occuperà di scaricare i nuovi file del *core* in particolare `lua/plugins/init.lua` e successivamente richiamerà `Lazy` che clonerà la nuova **ui**.

A questo punto alla prossima riapertura di NvChad dovresti avere tutto di nuovo a posto e aggiornato.
