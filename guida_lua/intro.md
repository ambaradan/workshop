1 - Introduzione

Lua è un linguaggio di scripting potente, efficiente, leggero e incorporabile. Supporta la programmazione procedurale, la programmazione orientata agli oggetti, la programmazione funzionale, la programmazione guidata dai dati e la descrizione dei dati.

Lua combina una semplice sintassi procedurale con potenti costrutti di descrizione dei dati basati su array associativi e una semantica estensibile. Lua è tipizzato dinamicamente, viene eseguito interpretando il bytecode con una macchina virtuale basata su registri e ha una gestione automatica della memoria con una garbage collection generazionale, che lo rende ideale per la configurazione, lo scripting e la prototipazione rapida.

Lua è implementato come libreria, scritta in clean C, il sottoinsieme comune di Standard C e C++. La distribuzione di Lua include un programma host chiamato lua, che utilizza la libreria Lua per offrire un interprete Lua completo e indipendente, per l'uso interattivo o batch. Lua è destinato a essere utilizzato sia come linguaggio di scripting potente, leggero e incorporabile per qualsiasi programma che ne abbia bisogno, sia come linguaggio autonomo potente ma leggero ed efficiente.

Come linguaggio di estensione, Lua non ha una nozione di programma "principale": funziona incorporato in un client host, chiamato programma incorporato o semplicemente host. (Il programma host può invocare funzioni per eseguire un pezzo di codice Lua, può scrivere e leggere variabili Lua e può registrare funzioni C per essere chiamate dal codice Lua. Attraverso l'uso di funzioni C, Lua può essere ampliato per far fronte a un'ampia gamma di domini diversi, creando così linguaggi di programmazione personalizzati che condividono una struttura sintattica.

Lua è software libero e viene fornito come sempre senza alcuna garanzia, come indicato nella sua licenza. L'implementazione descritta in questo manuale è disponibile presso il sito web ufficiale di Lua, www.lua.org.

Come ogni altro manuale di riferimento, questo documento è a tratti asciutto. Per una discussione delle decisioni alla base della progettazione di Lua, si vedano i documenti tecnici disponibili sul sito web di Lua. Per un'introduzione dettagliata alla programmazione in Lua, si veda il libro di Roberto, Programmare in Lua. 

2 - Concetti di base

Questa sezione descrive i concetti di base del linguaggio.
2.1 - Valori e tipi

Lua è un linguaggio tipizzato dinamicamente. Ciò significa che le variabili non hanno tipi, ma solo i valori. Non ci sono definizioni di tipo nel linguaggio. Tutti i valori hanno il loro tipo.

Tutti i valori in Lua sono valori di prima classe. Ciò significa che tutti i valori possono essere memorizzati in variabili, passati come argomenti ad altre funzioni e restituiti come risultati.

Ci sono otto tipi di base in Lua: nil, boolean, number, string, function, userdata, thread e table. Il tipo nil ha un unico valore, nil, la cui proprietà principale è quella di essere diverso da qualsiasi altro valore; spesso rappresenta l'assenza di un valore utile. Il tipo booleano ha due valori, false e true. Sia nil che false rendono falsa una condizione; sono chiamati collettivamente valori falsi. Qualsiasi altro valore rende una condizione vera. Nonostante il nome, false è spesso usato come alternativa a nil, con la differenza che false si comporta come un valore normale in una tabella, mentre nil rappresenta una chiave assente.

Il tipo number rappresenta sia numeri interi che numeri reali (a virgola mobile), utilizzando due sottotipi: integer e float. Lua standard utilizza numeri interi a 64 bit e float a doppia precisione (64 bit), ma è anche possibile compilare Lua in modo che utilizzi numeri interi a 32 bit e/o float a singola precisione (32 bit). L'opzione con 32 bit sia per gli interi che per i float è particolarmente interessante per le piccole macchine e i sistemi embedded. (Vedere la macro LUA_32BITS nel file luaconf.h).

A meno che non sia specificato diversamente, qualsiasi overflow durante la manipolazione di valori interi si avvolge su se stesso, secondo le consuete regole dell'aritmetica a due complementi. (In altre parole, il risultato effettivo è l'unico intero rappresentabile che è uguale modulo 2n al risultato matematico, dove n è il numero di bit del tipo intero).

Lua ha regole esplicite per l'utilizzo di ciascun sottotipo, ma effettua anche conversioni automatiche tra i vari tipi (vedere §3.4.3). Pertanto, il programmatore può scegliere di ignorare la differenza tra interi e float o di assumere un controllo completo sulla rappresentazione di ciascun numero.

Il tipo string rappresenta sequenze immutabili di byte. Lua è pulito a 8 bit: le stringhe possono contenere qualsiasi valore a 8 bit, compresi gli zeri incorporati ('\0'). Lua è anche indipendente dalla codifica; non fa alcuna ipotesi sul contenuto di una stringa. La lunghezza di una stringa in Lua deve essere contenuta in un intero Lua. 

Lua può chiamare (e manipolare) funzioni scritte in Lua e funzioni scritte in C (vedere §3.4.10). Entrambe sono rappresentate dal tipo function.

Il tipo userdata è fornito per consentire la memorizzazione di dati C arbitrari in variabili Lua. Un valore userdata rappresenta un blocco di memoria grezza. Ci sono due tipi di userdata: userdata completo, che è un oggetto con un blocco di memoria gestito da Lua, e userdata leggero, che è semplicemente un valore di puntatore C. Gli userdata non hanno operazioni predefinite in Lua, tranne l'assegnazione e il test di identità. Utilizzando i metataboli, il programmatore può definire le operazioni per i valori userdata completi (vedere §2.4). I valori degli userdata non possono essere creati o modificati in Lua, ma solo attraverso l'API C. Questo garantisce l'integrità dei dati di proprietà del programma host e delle librerie C.

Il tipo thread rappresenta i thread indipendenti di esecuzione e viene utilizzato per implementare le coroutine (vedere §2.6). I thread di Lua non sono correlati ai thread del sistema operativo. Lua supporta le coroutine su tutti i sistemi, anche quelli che non supportano i thread in modo nativo.

La tabella dei tipi implementa array associativi, cioè array che possono avere come indici non solo numeri, ma qualsiasi valore Lua tranne nil e NaN. (Not a Number è uno speciale valore in virgola mobile usato dallo standard IEEE 754 per rappresentare risultati numerici indefiniti, come 0/0). Le tabelle possono essere eterogenee, cioè possono contenere valori di tutti i tipi (tranne nil). Qualsiasi chiave associata al valore nil non è considerata parte della tabella. Viceversa, qualsiasi chiave che non fa parte di una tabella ha un valore associato nil. 

Le tabelle sono l'unico meccanismo di strutturazione dei dati in Lua; possono essere utilizzate per rappresentare array ordinari, liste, tabelle di simboli, insiemi, record, grafici, alberi, ecc. Per rappresentare i record, Lua utilizza il nome del campo come indice. Il linguaggio supporta questa rappresentazione fornendo a.name come zucchero sintattico per a["nome"]. Esistono diversi modi per creare tabelle in Lua (vedere §3.4.9).

Come gli indici, i valori dei campi delle tabelle possono essere di qualsiasi tipo. In particolare, poiché le funzioni sono valori di prima classe, i campi tabella possono contenere funzioni. Pertanto, le tabelle possono contenere anche metodi (vedere §3.4.11).

L'indicizzazione delle tabelle segue la definizione di uguaglianza grezza del linguaggio. Le espressioni a[i] e a[j] denotano lo stesso elemento di tabella se e solo se i e j sono uguali in modo grezzo (cioè uguali senza metametodi). In particolare, i float con valori integrali sono uguali ai rispettivi interi (ad esempio, 1.0 == 1). Per evitare ambiguità, qualsiasi float usato come chiave che sia uguale a un intero viene convertito in quell'intero. Ad esempio, se si scrive a[2.0] = true, la chiave effettiva inserita nella tabella sarà il numero intero 2.

Tabelle, funzioni, thread e valori (completi) dei dati utente sono oggetti: le variabili non contengono effettivamente questi valori, ma solo riferimenti ad essi. L'assegnazione, il passaggio di parametri e la restituzione di funzioni manipolano sempre riferimenti a tali valori; queste operazioni non implicano alcun tipo di copia.

La funzione type della libreria restituisce una stringa che descrive il tipo di un dato valore (vedere type).

2.2 - Ambienti e ambiente globale

Come si dirà più avanti nel §3.2 e nel §3.3.3, ogni riferimento a un nome libero (cioè non legato a nessuna dichiarazione) var viene sintatticamente tradotto in _ENV.var. Inoltre, ogni chunk viene compilato nell'ambito di una variabile locale esterna denominata _ENV (si veda §3.3.2), quindi _ENV stessa non è mai un nome libero in un chunk.

Nonostante l'esistenza di questa variabile esterna _ENV e la traduzione dei nomi liberi, _ENV è un nome del tutto regolare. In particolare, è possibile definire nuove variabili e parametri con questo nome. Ogni riferimento a un nome libero utilizza la _ENV visibile in quel momento nel programma, seguendo le consuete regole di visibilità di Lua (vedi §3.5).

Qualsiasi tabella utilizzata come valore di _ENV è chiamata ambiente.

Lua mantiene un ambiente distinto, chiamato ambiente globale. Questo valore è conservato in un indice speciale del registro C (vedere §4.3). In Lua, la variabile globale _G è inizializzata con questo stesso valore. (_G non viene mai usata internamente, quindi la modifica del suo valore avrà effetto solo sul vostro codice).

Quando Lua carica un chunk, il valore predefinito della sua variabile _ENV è l'ambiente globale (vedere load). Pertanto, per impostazione predefinita, i nomi liberi nel codice Lua si riferiscono a voci dell'ambiente globale e, quindi, sono anche chiamati variabili globali. Inoltre, tutte le librerie standard sono caricate nell'ambiente globale e alcune funzioni operano su tale ambiente. Si può usare load (o loadfile) per caricare un chunk con un ambiente diverso. (In C, si deve caricare il chunk e poi cambiare il valore del suo primo upvalue; vedere lua_setupvalue).

2.3 - Gestione degli errori

Diverse operazioni in Lua possono generare un errore. Un errore interrompe il normale flusso del programma, che può continuare catturando l'errore.

Il codice Lua può sollevare esplicitamente un errore chiamando la funzione error. (Questa funzione non ritorna mai).

Per catturare gli errori in Lua, è possibile effettuare una chiamata protetta, utilizzando pcall (o xpcall). La funzione pcall chiama una determinata funzione in modalità protetta. Qualsiasi errore durante l'esecuzione della funzione ne interrompe l'esecuzione e il controllo torna immediatamente a pcall, che restituisce un codice di stato.

Poiché Lua è un linguaggio di estensione incorporato, l'esecuzione del codice Lua inizia con una chiamata dal codice C del programma host. (Di solito, questa chiamata è protetta; quindi, quando si verifica un errore altrimenti non protetto durante la compilazione o l'esecuzione di un pezzo di Lua, il controllo torna all'host, che può prendere le misure appropriate, come la stampa di un messaggio di errore.

Ogni volta che si verifica un errore, viene propagato un oggetto errore con informazioni sull'errore. Lua stesso genera solo errori il cui oggetto errore è una stringa, ma i programmi possono generare errori con qualsiasi valore come oggetto errore. Spetta al programma Lua o al suo host gestire tali oggetti errore. Per ragioni storiche, un oggetto errore viene spesso chiamato messaggio di errore, anche se non deve essere necessariamente una stringa.

Quando si usa xpcall (o lua_pcall, in C) si può dare un gestore di messaggi da chiamare in caso di errori. Questa funzione viene chiamata con l'oggetto errore originale e restituisce un nuovo oggetto errore. Viene richiamata prima che l'errore sblocchi lo stack, in modo da poter raccogliere ulteriori informazioni sull'errore, per esempio ispezionando lo stack e creando uno stack traceback. Questo gestore di messaggi è ancora protetto dalla chiamata protetta; pertanto, un errore all'interno del gestore di messaggi richiamerà il gestore di messaggi. Se questo ciclo si protrae troppo a lungo, Lua lo interrompe e restituisce un messaggio appropriato. Il gestore di messaggi viene chiamato solo per i normali errori di runtime. Non viene chiamato per errori di allocazione della memoria o per errori durante l'esecuzione di finalizzatori o altri gestori di messaggi.

Lua offre anche un sistema di avvertimenti (vedere warn). A differenza degli errori, gli avvertimenti non interferiscono in alcun modo con l'esecuzione del programma. Generalmente generano solo un messaggio per l'utente, anche se questo comportamento può essere adattato dal C (vedere lua_setwarnf).

2.4 - Metataboli e metametodi

Ogni valore in Lua può avere un metatabolo. Questa metatabella è una normale tabella Lua che definisce il comportamento del valore originale in presenza di determinati eventi. È possibile modificare diversi aspetti del comportamento di un valore impostando campi specifici nella sua metatabella. Per esempio, quando un valore non numerico è l'operando di un'addizione, Lua verifica la presenza di una funzione nel campo __add della metatabella del valore. Se ne trova una, Lua chiama questa funzione per eseguire l'addizione.

La chiave per ogni evento in una metatabella è una stringa con il nome dell'evento preceduto da due trattini bassi; il valore corrispondente è chiamato metavalore. Per la maggior parte degli eventi, il metavalore deve essere una funzione, che viene chiamata metametodo. Nell'esempio precedente, la chiave è la stringa "__add" e il metametodo è la funzione che esegue l'addizione. A meno che non sia specificato diversamente, un metametodo può essere qualsiasi valore richiamabile, che sia una funzione o un valore con un metametodo __call.

È possibile interrogare il metametodo di qualsiasi valore utilizzando la funzione getmetatable. Lua interroga i metametodi nei metataboli usando un accesso grezzo (vedere rawget).

È possibile sostituire la metatabella delle tabelle utilizzando la funzione setmetatable. Non è possibile modificare la metatabella di altri tipi dal codice Lua, se non utilizzando la libreria di debug (§6.10).

Le tabelle e i dati utente completi hanno metataboli individuali, anche se più tabelle e dati utente possono condividere i loro metataboli. I valori di tutti gli altri tipi condividono una singola metatabella per tipo; cioè, c'è una singola metatabella per tutti i numeri, una per tutte le stringhe, ecc. Per impostazione predefinita, un valore non ha metataboli, ma la libreria delle stringhe imposta un metatabolo per il tipo di stringa (vedere §6.4).

Segue un elenco dettagliato delle operazioni controllate dai metataboli. Ogni evento è identificato dalla chiave corrispondente. Per convenzione, tutte le chiavi metatabili utilizzate da Lua sono composte da due trattini bassi seguiti da lettere latine minuscole.

__add: l'operazione di addizione (+). Se un operando di un'addizione non è un numero, Lua cercherà di chiamare un metametodo. Inizia controllando il primo operando (anche se è un numero); se questo operando non definisce un metametodo per __add, Lua controlla il secondo operando. Se Lua trova un metametodo, lo chiama con i due operandi come argomenti e il risultato della chiamata (adattato a un valore) è il risultato dell'operazione. Altrimenti, se non viene trovato alcun metametodo, Lua solleva un errore.
__sub: l'operazione di sottrazione (-). Comportamento simile all'operazione di addizione.
__mul: l'operazione di moltiplicazione (*). Comportamento simile all'operazione di addizione.
__div: operazione di divisione (/). Comportamento simile all'operazione di addizione.
__mod: operazione di modulo (%). Comportamento simile all'operazione di addizione.
__pow: operazione di esponenziazione (^). Comportamento simile all'operazione di addizione.
__unm: operazione di negazione (unario -). Comportamento simile all'operazione di addizione.
__idiv: operazione di divisione per piani (//). Comportamento simile all'operazione di addizione.
__band: operazione AND (&) bitwise. Comportamento simile all'operazione di addizione, tranne per il fatto che Lua tenterà un metametodo se un operando non è né un intero né un float coercibile con un intero (vedere §3.4.3).
__bor: l'operazione OR bitwise (|). Comportamento simile all'operazione AND bitwise.
__bxor: operazione di OR esclusivo bitwise (binario ~). Comportamento simile all'operazione AND bitwise.
__bnot: operazione bitwise NOT (unario ~). Comportamento simile a quello dell'operazione bitwise AND.
__shl: operazione bitwise left shift (<<). Comportamento simile all'operazione bitwise AND.
__shr: operazione di spostamento a destra bitwise (>>). Comportamento simile all'operazione bitwise AND.
__concat: operazione di concatenazione (..). Comportamento simile all'operazione di addizione, tranne per il fatto che Lua tenterà un metametodo se un operando non è né una stringa né un numero (che è sempre coercibile con una stringa).
__len: l'operazione di lunghezza (#). Se l'oggetto non è una stringa, Lua tenterà il suo metametodo. Se esiste un metametodo, Lua lo chiama con l'oggetto come argomento e il risultato della chiamata (sempre corretto a un valore) è il risultato dell'operazione. Se non esiste un metametodo ma l'oggetto è una tabella, Lua utilizza l'operazione di lunghezza della tabella (vedere §3.4.7). Altrimenti, Lua solleva un errore.
__eq: l'operazione di uguaglianza (==). Comportamento simile all'operazione di addizione, tranne per il fatto che Lua tenterà un metametodo solo quando i valori da confrontare sono entrambi tabelle o entrambi dati utente completi e non sono primitivamente uguali. Il risultato della chiamata viene sempre convertito in un booleano.
__lt: l'operazione meno di (<). Comportamento simile all'operazione di addizione, con la differenza che Lua tenterà un metametodo solo quando i valori da confrontare non sono né entrambi numeri né entrambi stringhe. Inoltre, il risultato della chiamata viene sempre convertito in un booleano.
__le: l'operazione meno uguale (<=). Comportamento simile all'operazione meno di.
__index: L'operazione di accesso all'indicizzazione tabella[chiave]. Questo evento si verifica quando la tabella non è una tabella o quando la chiave non è presente nella tabella. Il metavalore viene cercato nella metatabella della tabella.

Il metavalore per questo evento può essere una funzione, una tabella o qualsiasi valore con un metavalore __index. Se è una funzione, viene chiamata con tabella e chiave come argomenti e il risultato della chiamata (adattato a un valore) è il risultato dell'operazione. Altrimenti, il risultato finale è il risultato dell'indicizzazione di questo metavalore con la chiave. Questa indicizzazione è regolare, non grezza, e quindi può attivare un altro metavalore __index.
__newindex: L'assegnazione dell'indicizzazione tabella[chiave] = valore. Come l'evento index, questo evento si verifica quando la tabella non è una tabella o quando la chiave non è presente nella tabella. Il metavalore viene cercato nella metatabella della tabella.

Come per l'indicizzazione, il metavalore per questo evento può essere una funzione, una tabella o qualsiasi valore con un metavalore __newindex. Se è una funzione, viene richiamata con tabella, chiave e valore come argomenti. Altrimenti, Lua ripete l'assegnazione dell'indicizzazione su questo metavalore con la stessa chiave e lo stesso valore. Questo assegnamento è regolare, non grezzo, e quindi può attivare un altro metavalore __newindex.

Ogni volta che viene invocato un metavalore __newindex, Lua non esegue l'assegnazione primitiva. Se necessario, il metametodo stesso può chiamare rawset per eseguire l'assegnazione.
__call: L'operazione di chiamata func(args). Questo evento si verifica quando Lua tenta di chiamare un valore non funzione (cioè, func non è una funzione). Il metametodo viene cercato in func. Se presente, il metametodo viene chiamato con func come primo argomento, seguito dagli argomenti della chiamata originale (args). Tutti i risultati della chiamata sono i risultati dell'operazione. Questo è l'unico metametodo che consente di ottenere più risultati.

Oltre all'elenco precedente, l'interprete rispetta anche le seguenti chiavi nei metataboli: __gc (vedere §2.5.3), __close (vedere §3.3.8), __mode (vedere §2.5.4) e __name. (La voce __name, quando contiene una stringa, può essere utilizzata da tostring e nei messaggi di errore).

Per gli operatori unari (negazione, lunghezza e bitwise NOT), il metametodo viene calcolato e chiamato con un secondo operando fittizio, uguale al primo. Questo operando extra serve solo a semplificare l'interno di Lua (facendo sì che questi operatori si comportino come un'operazione binaria) e potrebbe essere rimosso nelle versioni future. Per la maggior parte degli usi questo operando extra è irrilevante.

Poiché le metatabelle sono tabelle regolari, possono contenere campi arbitrari, non solo i nomi degli eventi definiti sopra. Alcune funzioni della libreria standard (ad esempio, tostring) utilizzano altri campi delle metatabelle per i loro scopi.

È buona norma aggiungere tutti i metametodi necessari a una tabella prima di impostarla come metatabella di un oggetto. In particolare, il metametodo __gc funziona solo se viene seguito questo ordine (vedere §2.5.3). È anche una buona pratica impostare la metatabella di un oggetto subito dopo la sua creazione. 
