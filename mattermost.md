# Mattermost Chat

Again, this is all normal although difficult to interpret without a basic knowledge of MkDocs. Let us start by fixing two points:

1. There is no tag consisting of three exclamation points `!!!` in the Markdown Standard this is a "plus" provided by the *material* theme used in MkDocs for creating the static site.

2. in the conversion from Markdown to HTML all code contained in a block of code is reproduced exactly as written in the Markdown document, nothing is interpreted.

Instead that particular block of code produces code with a clickable link and is surrounded by a beautiful colored title frame, how come if we just said that everything in a block of code is converted without interpretation?

This is due to the use of the `material-theme` of MkDocs that provides this functionality, the same page displayed on a Markdown preview or even on an instance of MkDocs with a different theme produces what you see in the preview. Because you must not forget that that is just a preview, an aid, it does not reflect the final document, to check the final code that is produced you can use `Ctrl + P` that is the real result of what will be uploaded to the documentation site.

In conclusion what you see is what can be interpreted using the Markdown Standard, if there are parts that can be interpreted only with external add-ons these are not interpreted and consequently the link that on the documentation is interpreted in the editor preview is displayed in full.

References

- [Mkdocs](https://www.mkdocs.org/) MkDocs is a fast, simple and downright gorgeous static site generator
- [Material-theme](https://squidfunk.github.io/mkdocs-material/) Documentation that simply works
- [Markdown Guide](https://www.markdownguide.org/) The Markdown Guide is a free and open-source reference guide that explains how to use Markdown

Another aspect to evaluate is the transformation undergone by the frontmatter:

```yaml
---
author: Einstein
contributors: 'Dr. Ben Dover, Sweet Gypsy Rose'
tags:
  - sample
  - crowdin
  - markdown
'tested with': 8.5
title: 'Markdown Demo'
---
```

As you can see all strings with a space are enclosed between two quotes and while this should not be a problem for the content it might be a problem with `tested with` you will have to check if the tag is interpreted correctly when building the site with MkDocs.

By modifying the frontmatter sencording to Yaml rules the result is as follows:

```yaml
---
title: Markdown Demo
author: Einstein
contributors: Dr. Ben Dover,Sweet Gypsy Rose
tested_with: 8.5
tags:
  - sample
  - crowdin
  - markdown
---
```

Although allowed the use of spaces in the key name is discouraged, and you can see why with `tested_with`, the contents of keys are not strings but rather variables so after the separating comma in the contributor field a space should not be put. I think it is a case of setting a standard for writing documentation frontmatter as well.

